<?php namespace ironwoods\modules\messages\entities;


/**
 * @file: owner.php
 * @info: Class for create owners
 *
 * @utor: Moisés Alcocer
 * 2017, <m_alc1@hotmail.com>
 * http://www.ironwoods.es
 */

class Owner {
	
	/**********************************/
	/*** Properties declaration *******/

		private $name 	= NULL;
		private $email 	= NULL;
		private $created_at = NULL;
		private $deleted_at = NULL;


	/**********************************/
	/*** Methods declaration **********/

		/**
		 * Construct
		 *
		 * @param   string  $name
		 * @param   string  $email
		 */
		public function __construct(
			$name=NULL,
			$email=NULL
		) {

			$this->name = $name;
			$this->email = $email;
		}

	/*** Public Methods ***************/

	////////////////////////////////////////////////////////////////////////////
	// Getters
	//
	//

		public function getName() {

			return $this->name;
		}

		public function getEmail() {

			return $this->email;
		}
		
		public function getCreationData() {

			return $this->created_at;
		}

		public function getDeletionData() {

			return $this->deleted_at;
		}
		

	////////////////////////////////////////////////////////////////////////////
	// Setters
	//
	//

		public function setName( $x ) {
			$this->name = $x;

			return $this;
		}

		public function setEmail( $x ) {
			$this->email = $x;

			return $this;
		}

		public function setCreationData() {
			$this->created_at = now();

			return $this;
		}

		public function setDeletionData() {
			$this->deleted_at = now();

			return $this;
		}


	/*** Private Methods **************/


} //class