<?php namespace ironwoods\modules\messages\entities;


/**
 * @file: message.php
 * @info: Class for ...
 *
 * @utor: Moisés Alcocer
 * 2017, <m_alc1@hotmail.com>
 * http://www.ironwoods.es
 */

class Message {
	
	/**********************************/
	/*** Properties declaration *******/

		private $id 		= NULL;
		private $sender_id 	= NULL;
		private $receiver_id = NULL;
		private $subject 	= NULL;
		private $content 	= NULL;
		private $read 		= NULL;
		private $created_at = NULL;
		private $deleted_at = NULL;


	/**********************************/
	/*** Methods declaration **********/

		/**
		 * Construct
		 *
		 * @param	int	$writter_id
		 * @param   string  $subject  The subject of message
		 * @param   string  $content  The content of message
		 */
		public function __construct(
			$writter_id=NULL,
			$subject=NULL,
			$content=NULL
		) {

			$this->sender_id = $writter_id;
			$this->subject = $subject;
			$this->content = $content;
		}

	/*** Public Methods ***************/

	////////////////////////////////////////////////////////////////////////////
	// Getters
	//
	//

		public function getSenderId() {

			return $this->sender_id;
		}

		public function getSubject() {

			return $this->subject;
		}

		public function getContent() {

			return $this->content;
		}
	

	////////////////////////////////////////////////////////////////////////////
	// Setters
	//
	//
	
		public function setSenderId( $x ) {
			$this->sender_id = $x;

			return $this;
		}

		public function setSubject( $x ) {
			$this->subject = $x;

			return $this;
		}

		public function setContent( $x ) {
			$this->content = $x;

			return $this;
		}



	/*** Private Methods **************/


} //class