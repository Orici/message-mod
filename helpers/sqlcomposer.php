<?php namespace ironwoods\modules\messages\helpers;

/**
 * @file: sqlcomposer.php
 * @info: Class to compose SQL queries
 *
 * @utor: Moisés Alcocer
 * 2017, <m_alc1@hotmail.com>
 * http://www.ironwoods.es
 */

use \ironwoods\modules\messages\entities\Message as Message;
use \ironwoods\modules\messages\entities\Owner as Owner;


class SqlComposer {
	
	/**********************************/
	/*** Properties declaration *******/

		private static $class = "SqlComposer";


	/**********************************/
	/*** Methods declaration **********/

	/*** Public Methods ***************/

		/**
		 * Gets the parameters to an INSERT from a message
		 *
		 * @param      Message  $msg
		 * @return     array
		 */
		public static function getParams( Message $msg ) {
			//prob( self::$class . " / getParams()" );

			$sender_id 	= $msg->getSenderId();
			$subject 	= $msg->getSubject();
			$content 	= $msg->getContent();

			$params = [
				$sender_id,
				$subject,
				$content,
			];
			

			return $params;
		}

		public static function insertMsg( $table, Message $msg ) {
			//prob( self::$class . " / insertMsg()" );

			$sender_id 	= $msg->getSenderId();
			$subject 	= $msg->getSubject();
			$content 	= $msg->getContent();

			$sql = "INSERT INTO {$table} VALUES( NULL, ";
			$sql .= $sender_id . ", ";
			$sql .= "NULL, ";
			$sql .= "\"" . $subject . "\", ";
			$sql .= "\"" . $content . "\", ";
			$sql .= 0;
			$sql .= ", \"" . now() . "\", ";
			$sql .= 0;
			$sql .= ")";

			
			return $sql;
		}

		public static function insertMsgWithParams( $table, Message $msg ) {
			//prob( self::$class . " / insertMsgWithParams()" );

			$sql = "INSERT INTO {$table} 
				VALUES( NULL, ?, NULL, ?, ?, 0, \"" . now() . "\", 0 )";
			////prob( $sql );

			
			return $sql;
		}

		public static function insertOwner( $table, Owner $owner ) {
			//prob( self::$class . " / insertOwner()" );

			$name 	= $owner->getName();
			$email 	= $owner->getEmail();

			$sql = "INSERT INTO {$table} VALUES( NULL, ";
			$sql .= "\"" . $name . "\", ";
			$sql .= "\"" . $email . "\", ";
			$sql .= "\"" . now() . "\", ";
			$sql .= 0;
			$sql .= ")";

			
			return $sql;
		}


	/*** Private Methods **************/



} //class
