<?php namespace ironwoods\modules\messages\helpers;

/**
 * @file: paramsvalidator.php
 * @info: Class that implements method for validations
 *
 * @utor: Moisés Alcocer
 * 2017, <m_alc1@hotmail.com>
 * http://www.ironwoods.es
 */

//use \ironwoods\modules\messages\entities\Message as Message;
//use \ironwoods\modules\messages\entities\Owner as Owner;


class ParamsValidator {

	/**********************************/
	/*** Properties declaration *******/

		private static $class = "ParamsValidator";


	/**********************************/
	/*** Methods declaration **********/

	/*** Public Methods ***************/

		public static function connection( $x=NULL ) {

			return ( $x && is_a( $x, "PDO" ));
		}

		/**
		 * Determines if the instance is of class Message
		 *
		 * @param      object	$entity
		 * @return     boolean
		 */
		public static function isMessage( $entity=NULL ) {
			//prob( self::$class . " / isMessage()" );

			if ( $entity && is_object( $entity ))
				return is_a( $entity , '\ironwoods\modules\messages\entities\Message' );

			err( self::$class . " / isMessage() -> Err args" );


			return FALSE;
		}

		/**
		 * Determines if the instance is of class Owner
		 *
		 * @param      object	$entity
		 * @return     boolean
		 */
		public static function isOwner( $entity=NULL ) {
			//prob( self::$class . " / isOwner()" );

			if ( $entity && is_object( $entity ))
				return is_a( $entity , '\ironwoods\modules\messages\entities\Owner' );

			err( self::$class . " / isOwner() -> Err args" );


			return FALSE;
		}

		/**
		 * Determines if the array has complete settings data
		 *
		 * @param      array	$arr_data
		 * @return     boolean
		 */
		public static function settingsData( $arr_data ) {
			//var_dump( $arr_data ); //die();

			//Array vacio es tratado como FALSE)
			if ( $arr_data && is_array( $arr_data )) {

				return ( array_key_exists( "table-prefix" , $arr_data )
					&& array_key_exists( "table" ,		$arr_data )
					&& array_key_exists( "id" , 		$arr_data )
					&& array_key_exists( "id-type" ,	$arr_data )
					&& array_key_exists( "id-length",	$arr_data )
					&& array_key_exists( "id-unsigned",	$arr_data ));
			}
			err( self::class . " / settingsData() -> Err args" );


			return FALSE;
		}

		public static function message( $x=NULL ) {

			return ( self::isMessage( $x ) && self::hasNecessaryData( $x ));
		}

		public static function id( $x=NULL ) {
			$x = (int) $x;
			
			return ( (int)$x && is_int( $x ));
		}


	/*** Private Methods **************/

		/**
		 * Determines if the message has necessary data
		 *
		 * @param	  Message   $msg
		 * @return	 boolean
		 */
		private static function hasNecessaryData( $msg ) {
			
			$sender_id	= $msg->getSenderId();
			$subject	= $msg->getSubject();
			$content	= $msg->getContent();


			return ( $sender_id && $subject && $content );
		}
	
} //class

//is_a() -> Comprueba si un objeto es de una clase o tiene esta 
//clase como una de sus madres. Si una clase pertenece a un namespace
//indicar este completo y SIEMPRE entre comillas simples, p.e.
// '\ironwoods\entities\Message'
