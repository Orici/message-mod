<?php namespace ironwoods\modules\messages\helpers;

/**
 * @file: dbvalidator.php
 * @info: Class with methods for validations againts the DB
 *
 * @utor: Moisés Alcocer
 * 2017, <m_alc1@hotmail.com>
 * http://www.ironwoods.es
 */

class DBValidator {

	/**********************************/
	/*** Properties declaration *******/

		private static $class = "DBValidator";


	/**********************************/
	/*** Methods declaration **********/

	/*** Public Methods ***************/

		/**
		 * Searchs for the table in the DB
		 *
		 * @param      object	$con
		 * @param      string	$table
		 * @return     boolean
		 */
		public static function existTable( $con=NULL, $table=NULL ) {
			//prob( self::$class . " / existTable() -> " . $table );

			if ( ParamsValidator::connection( $con ) && $table ) {

				$sql = "SELECT COUNT(*) as count 
					FROM information_schema.TABLES 
					WHERE Table_Name='{$table}'";

				$res = self::runQuery( $con, $sql )->fetch()->count;


				return ( $res != 0 );
			}

			err( self::$class . " / existTable() -> Err args", TRUE );
		}
	

	/*** Private Methods **************/

		/**
		 * Runs a query againts the DB and returns the results
		 *
		 * @param		string		$sql
		 * @return  	mixed
		 */
		private static function runQuery( $con, $sql ) {
			//prob( self::$class . " / runQuery ()" );
			//prob( "SQL: <span style='color:blue'>" . $sql . "</span>" );
			
			$query = $con->prepare( $sql );
			
			if ( $query )
				$query->execute();
			
			else
				err( self::$class . " / runQuery() -> Err: Query vacia" );


			return $query;
		}
} //class