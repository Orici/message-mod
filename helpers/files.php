<?php namespace ironwoods\modules\messages\helpers;

/**
 * @file: files.php
 * @info: Class for handling files
 *
 * @utor: Moisés Alcocer
 * 2017, <m_alc1@hotmail.com>
 * http://www.ironwoods.es
 */


class Files {
	
	/**********************************/
	/*** Properties declaration *******/

		private static $class = "Files";


	/**********************************/
	/*** Methods declaration **********/

	/*** Public Methods ***************/

		/**
		 * Gets the content.
		 *
		 * From: ACM helpers/ficherosedit.php
         * 
         * @param   string   $file_path (path+nombre+tipo p.e. ./cosas/lunes.php)
         * @return  mixed
         */ 
        public static function getContent( $file_path=NULL ) {
            $head = self::$class . " / getContent()";
            //prob( $head ." -> file: {$file_path}" ); //traza
            

            if ( ! $file_path || ! is_string( $file_path )) {
                //prob( $head . " -> Err args" );
                
                return FALSE;   
            }

            if ( ! file_exists( $file_path )) {
                //prob( $head . " -> Err: fichero NO existe: {$file_path}" );

                return FALSE;
            }
               
            //Abrir fichero
            $file = fopen( $file_path, "r" );
            
            //Se abrio el fichero
            if ( $file ) {
                $cad = "";

                //Lee el fichero linea por linea
                while( !feof( $file ))
                  $cad .= fgets( $file );

                //Cierra el fichero
                fclose( $file );

                //Devuelve contenido
                return $cad;
            }
                
            //No pudo abrirse el fichero //false
            //prob( $head . " -> Err. abriendo fichero: {$file_path}" );

                
            
            return FALSE;
        }

        public static function write( $file_path=NULL, $str=NULL ) {
            //prob( self::$class . " / write()" );
            ////prob( "Actual directory: " . getcwd() . "<hr>" );

            if ( is_string( $file_path ) && is_string( $str )) {
                $res = file_put_contents( 
                    $file_path, 
                    $str . PHP_EOL 
                );

                //file_put_contents devuelve FALSE pero tb puede devolver un resultado que se evaluaría como FALSE
                if ( $res !== FALSE )
                    return TRUE;

                //prob( self::$class . " / write() -> Err: no se pudo escribir el fichero" );

            } else
                //prob( self::$class . " / write() -> Err args" );


            return FALSE;
        }

	/*** Private Methods **************/



} //class
