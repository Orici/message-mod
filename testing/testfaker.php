<?php namespace ironwoods\modules\messages\testing;

/**
 * @file: testfaker.php
 * @info: Class for testing the class Faker
 *
 * @utor: Moisés Alcocer
 * 2017, <m_alc1@hotmail.com>
 * http://www.ironwoods.es
 */

use \ironwoods\modules\messages\entities\Message as Message;
use \ironwoods\modules\messages\entities\Owner as Owner;
use \ironwoods\modules\messages\model\Faker as Faker;


class TestFaker {
	
	/**********************************/
	/*** Properties declaration *******/

		private $class 	= "TestFaker";
		
		private $faker = NULL;


	/**********************************/
	/*** Methods declaration **********/

		public function __construct( $n_test=NULL ) {
			//prob( $this->class . " / __construct() <hr>" );

			$sdbcon = new \ironwoods\common\mysql\SimpleDbCon();
			$sdbcon->setDbName( "test-messages-mod" )
				->setDbUser( "root" )
				->setDbPass( "" );

			$this->faker = new Faker( $sdbcon->getConnection( ));
			$this->run( $n_test );
		}

	/*** Public Methods ***************/

		public function run( $n_test ) {
			//prob( $this->class . " / run() -> Runing Test...<br>" );

			if ( $this->faker ) {

				if ( $n_test == 1 )
					$this->test1();

				if ( $n_test == 2 )
					$this->test2();

				if ( $n_test == 3 )
					$this->test3();

				if ( $n_test == 4 )
					$this->test4();

			} else
				err( "Err -> Faker instance hasn´t created", TRUE );
		}

	/*** Private Methods **************/

		/**
		 * Test 1
		 * 
		 */
		private function test1() {
			//prob( "Runs Test 1 -> " . $this->class );
			//prob( "<h3 class='green'>Testing method: <b>prepareDb()</b></h3>" );

			$this->faker->prepareDb();
		}

		/**
		 * Test 2
		 * 
		 */
		private function test2() {
			//prob( "Runs Test 2 -> " . $this->class );
			//prob( "<h3 class='green'>Testing method: <b>seed()</b></h3>" );

			$this->faker->seed();
		}

		/**
		 * Test 3
		 * 
		 */
		private function test3() {
			//prob( "Runs Test 3 -> " . $this->class );
			//prob( "<h3 class='green'>Testing method: <b>renove()</b></h3>" );

			$this->faker->renove();
		}

		/**
		 * Test 4
		 * 
		 */
		private function test4() {
			//prob( "Runs Test 4 -> " . $this->class );
			//prob( "<h3 class='green'>Testing method: <b>put()</b></h3>" );

			$msg 	= new Message( 2, "the subject", "content for the message" );
			$owner  = new Owner( "Xxx", "xxx@mail.com" );
			$this->faker->put( $msg );
			$this->faker->put( $owner );
		}

} //class
