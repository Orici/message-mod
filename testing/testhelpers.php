<?php namespace ironwoods\modules\messages\testing;

/**
 * @file: testhelpers.php
 * @info: Class for testing the helpers
 *
 * @utor: Moisés Alcocer
 * 2017, <m_alc1@hotmail.com>
 * http://www.ironwoods.es
 */

use \ironwoods\modules\messages\entities\Message as Message;
use \ironwoods\modules\messages\entities\Owner as Owner;
use \ironwoods\modules\messages\helpers\DBValidator as DBVal;
use \ironwoods\modules\messages\helpers\Files as Files;
use \ironwoods\modules\messages\helpers\ParamsValidator as PVal;
use \ironwoods\modules\messages\helpers\SqlComposer as SqlComposer;


class TestHelpers {
	
	/**********************************/
	/*** Properties declaration *******/

		private $class 	= "TestHelpers";
		
		private $con = NULL;


	/**********************************/
	/*** Methods declaration **********/

		public function __construct( $class=NULL, $n_test=NULL ) {
			//prob( $this->class . " / __construct() <hr>" );

			$sdbcon = new \ironwoods\common\mysql\SimpleDbCon();
			$sdbcon->setDbName( "test-messages-mod" )
				->setDbUser( "root" )
				->setDbPass( "" );

			$this->con = $sdbcon->getConnection();
			$this->run( $class, $n_test );
		}

	/*** Public Methods ***************/

		public function run( $class, $n_test ) {
			//prob( $this->class . " / run() -> Runing Test...<br>" );

			if ( $this->con ) {
				echo "<pre>";

				if ( $class == "ParamsValidator" ) {

					if ( $n_test == 1 )
						$this->testPVal1();

					if ( $n_test == 2 )
						$this->testPVal2();

					if ( $n_test == 3 )
						$this->testPVal3();
				}

				if ( $class == "SqlComposer" ) {
					
					if ( $n_test == 1 )
						$this->testSqlComposer1();

					if ( $n_test == 2 )
						$this->testSqlComposer2();
				}
				/*
				if ( $class == "SqlComposer" ) {
					
					if ( $n_test == 1 )
						$this->test1();

					if ( $n_test == 2 )
						$this->test2();

					if ( $n_test == 3 )
						$this->test3();
				}*/

			} else
				err( "Err -> Db connection don´t exist", TRUE );
		}

	/*** Private Methods **************/

		/**
		 * Test - ParamsValidator
		 * 
		 */
		private function testPVal1() {
			//prob( "Runs Test ParamsValidator 1 / Method connection()" );

			$res = PVal::connection( $this->con );
			var_dump( $res ); 
			echo "<hr>";
		}

		private function testPVal2() {
			//prob( "Runs Test ParamsValidator 2 -> " . $this->class );

			$entity = new Message( 2, "Xxx", "content xxx" );

			$res = PVal::isMessage( $entity );
			var_dump( $res );
			echo "<hr>"; 
		}

		private function testPVal3() {
			//prob( "Runs Test ParamsValidator 3 -> " . $this->class );

			$entity = new Owner( "Xxx", "xxx@mail.com" );

			$res = PVal::isOwner( $entity );
			var_dump( $res );
			echo "<hr>";
		}
		/////////////////////////////////////////////////////////////// 
		
		/**
		 * Test SqlComposer
		 * 
		 */
		private function testSqlComposer1() {
			//prob( "Runs Test SqlComposer / insertMsg()" );

			$entity = new Message( 2, "Xxx", "content xxx" );

			$res = SqlComposer::insertMsg( "messages", $entity );
			var_dump( $res );
			echo "<hr>";
		}

		private function testSqlComposer2() {
			//prob( "Runs Test SqlComposer / insertOwner()" );

			$entity = new Owner( "Xxx", "xxx@mail.com" );

			$res = SqlComposer::insertOwner( "owners", $entity );
			var_dump( $res );
			echo "<hr>"; 
		}
		///////////////////////////////////////////////////////////////

		/**
		 * Test 1
		 * 
		 */
		private function test1() {
			//prob( "Runs Test 1 -> " . $this->class );


		}

		/**
		 * Test 2
		 * 
		 */
		private function test2() {
			//prob( "Runs Test 2 -> " . $this->class );


		}

		/**
		 * Test 3
		 * 
		 */
		private function test3() {
			//prob( "Runs Test 3 -> " . $this->class );


		}

} //class
