<?php namespace ironwoods\common\mysql;

/**
 * @file: simpledbcon.php
 * @info: Creates a DB connection with MySql / PDO
 * The class that need a connection must be extend this class
 *
 * @utor: Moisés Alcocer
 * 2017, <m_alc1@hotmail.com>
 * http://www.ironwoods.es
 * 
 * @package	   app.core
 */


class SimpleDbCon {

	/**********************************/
	/*** Properties declaration *******/

		private $class		= '@SimpleDbCon';

		private $db_name	= NULL;
		private $db_pass	= "";
		private $db_user	= "root";

		private $env		= "test"; // dev | test | production (environment)


	/**********************************/
	/*** Methods declaration **********/

		/**
		 * Constructor
		 * Every new instance has a connection opened with the database
		  * 
		 */
		public function __construct( $db_name=NULL, $db_user=NULL, $db_pass=NULL ) {
			
			$this->db_name = $db_name;
			$this->db_user = $db_user;
			$this->db_pass = $db_pass;
		}


	/*** Public Methods ***************/
		
		/**
		 * Sets the database name.
		 *
		 * @param 		string		$str
		 * @return	object
		 */
		public function setDbName( $str=NULL ) {

			if ( $str && is_string( $str )) {

				$this->db_name = $str;
			}


			return $this;
		}

		/**
		 * Sets the database pass.
		 *
		 * @param 		string		$str
		 * @return	object
		 */
		public function setDbPass( $str=NULL ) {

			if ( $str && is_string( $str )) {

				$this->db_pass = $str;
			}


			return $this;
		}

		/**
		 * Sets the database user.
		 *
		 * @param 		string		$str
		 * @return	object
		 */
		public function setDbUser( $str=NULL ) {

			if ( $str && is_string( $str )) {

				$this->db_user = $str;
			}


			return $this;
		}

		/**
		 * Opens and returns a DB connection using PDO
		 * 
		 * @return	object
		 */
		public function getConnection( $db_host=NULL ) {
			
			$db_host = ( $db_host ) ? $db_host : "localhost";

			// set the (optional) options of the PDO connection.
			// For example, fetch mode FETCH_ASSOC would return results like this:  $result[ "user_name ]
			// For example, fetch mode FETCH_OBJ would return results like this:	$result->user_name
			// 
			// @see http://www.php.net/manual/en/pdostatement.fetch.php

			$options = [
				\PDO::ATTR_DEFAULT_FETCH_MODE 	=> \PDO::FETCH_OBJ,
				\PDO::ATTR_EMULATE_PREPARES 	=> FALSE, //Inactives emulated prepares
				\PDO::MYSQL_ATTR_INIT_COMMAND 	=> "SET NAMES utf8"
			];
			$options[] = ( $this->env === ( 'dev' || 'test' ))
				? array( \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION )
				: array( \PDO::ATTR_ERRMODE => \PDO::ERRMODE_SILENT );

			try {
				return new \PDO( 
					'mysql:host=' . $db_host . 
					';dbname=' . $this->db_name, 
					$this->db_user, 
					$this->db_pass, 
					$options 
				);

			} catch ( \PDOException $e ) {

				//Shows an error and finish execution
				die( "Error de conexión a la BD." . $e->getMessage( ));
			}
		}


	/*** Private Methods **************/



} //class
