<?php namespace ironwoods\modules\messages\testing;

/**
 * @file: testfacade.php
 * @info: Class for testing the module main class
 *
 * @utor: Moisés Alcocer
 * 2017, <m_alc1@hotmail.com>
 * http://www.ironwoods.es
 */

use \ironwoods\modules\messages\Messages as Messages;


class TestFacade {
	
	/**********************************/
	/*** Properties declaration *******/

		private $class 	= "TestFacade";
		private $header_test = "<h2 class='green'>Runs Test: TestFacade";

		private $con = NULL;
		private $facade = NULL;


	/**********************************/
	/*** Methods declaration **********/

		public function __construct( $test=NULL, $n_test=NULL ) {
			//prob( $this->class . " / __construct() <hr>" );

			$sdbcon = new \ironwoods\common\mysql\SimpleDbCon();
			$sdbcon->setDbName( "test-messages-mod" )
				->setDbUser( "root" )
				->setDbPass( "" );

			$this->facade = new Messages( $sdbcon->getConnection( ));
			$this->run( $test, $n_test );
		}

	/*** Public Methods ***************/

		public function run( $test, $n_test ) {
			//prob( $this->class . " / run() -> Runing Test...<br>" );

			if ( $this->facade ) {

				if ( $test == "install" )
					$this->testInstall();

				if ( $test == "uninstall" )
					$this->testUninstall();

				if ( $test == "countMsgs" )
					$this->testCountMsgs();

				if ( $test == "countNewMsgs" )
					$this->testCountNewMsgs();

				/**
				 * Drafts
				 * 
				 */
				if ( $test == "addOneDraft" )
					$this->testAddOneDraft();

				if ( $test == "changeDraftContent" )
					$this->testChangeDraftContent();

				if ( $test == "changeDraftSubject" )
					$this->testChangeDraftSubject();

				if ( $test == "deleteOneDraft" )
					$this->testDeleteOneDraft();

				if ( $test == "getAllDrafts" )
					$this->testGetAllDrafts();

				if ( $test == "getOneDraft" )
					$this->testGetOneDraft();

				/**
				 * Messages
				 * 
				 */
				if ( $test == "deleteOne" )
					$this->testDeleteOne();

				if ( $test == "getAll" )
					$this->testGetAll();

				if ( $test == "getOne" )
					$this->testGetOne();

				if ( $test == "getUnread" )
					$this->testGetUnread();

				if ( $test == "send" )
					$this->testSend();

				if ( $test == "setRead" )
					$this->testsetRead();

			} else
				err( "Err -> Db connection don´t exist", TRUE );
		}

	/*** Private Methods **************/

		/**
		 * Test / Service -> installer()
		 * 
		 */
		private function testInstall() {
			//prob( $this->header_test . " / testInstall()</h2>" );

			$arr = [
				"table-prefix"	=> "test_",
				"table"			=> "owners",
				"id"			=> "id",
				"id-type" 		=> "INT",
				"id-length" 	=> 10,
				"id-unsigned"	=> TRUE,
			];

			$this->facade->install( $arr );
		}

		/**
		 * Test / Service -> uninstaller()
		 * 
		 */
		private function testUninstall() {
			//prob( $this->header_test . " / testUninstall()</h2>" );

			$this->facade->uninstall();
		}

		/**
		 * Test / countMsgs()
		 * 
		 */
		private function testCountMsgs() {
			//prob( $this->header_test . " / testCountMsgs()</h2>" );

			dx( $this->facade->countMsgs( 1 ));
		}

		/**
		 * Test / countNewMsgs()
		 * 
		 */
		private function testCountNewMsgs() {
			//prob( $this->header_test . " / testCountNewMsgs()</h2>" );

			dx( $this->facade->countNewMsgs( 1 ));
		}


	////////////////////////////////////////////////////////////////////////////
	// Methods in class "XDrafts"
	//
	//
		/**
		 * Test / addOneDraft()
		 * 
		 */
		private function testAddOneDraft() {
			//prob( $this->header_test . " / testAddOneDraft()</h2>" );


			$draft = new \ironwoods\modules\messages\entities\Message(
				1,
				"Una prueba...",
				"Zzzzzzzzzzzzzzzzzzzzzz !!"
			);

			dx( $this->facade->addOneDraft( $draft ));
		}

		/**
		 * Test / changeDraftContent()
		 * 
		 */
		private function testChangeDraftContent() {
			//prob( $this->header_test . " / testChangeDraftContent()</h2>" );

			dx( $this->facade->changeDraftContent( 1, "Nuevo contenido msg ID: 1" ));
		}

		/**
		 * Test / changeDraftSubject()
		 * 
		 */
		private function testChangeDraftSubject() {
			//prob( $this->header_test . " / testChangeDraftSubject()</h2>" );

			dx( $this->facade->changeDraftSubject( 1, "Nuevo encabezado msg ID: 1" ));
		}

		/**
		 * Test / deleteOneDraft()
		 * 
		 */
		private function testDeleteOneDraft() {
			//prob( $this->header_test . " / testDeleteOneDraft()</h2>" );

			dx( $this->facade->deleteOneDraft( 1, 1 ));
			dx( $this->facade->deleteOneDraft( 1, 2 ));
		}

		/**
		 * Test / getAllDrafts()
		 * 
		 */
		private function testGetAllDrafts() {
			//prob( $this->header_test . " / testGetAllDrafts()</h2>" );

			dx( $this->facade->getAllDrafts( 1 ));
		}

		/**
		 * Test / getOneDraft( $id, $writer_id )
		 * 
		 */
		private function testGetOneDraft() {
			//prob( $this->header_test . " / testGetOneDraft()</h2>" );

			dx( $this->facade->getOneDraft( 1, 1 )); //$id, $writer_id
			dx( $this->facade->getOneDraft( 2, 1 ));
			dx( $this->facade->getOneDraft( 3, 1 ));
		}


	////////////////////////////////////////////////////////////////////////////
	// Methods in class "XMessages"
	//
	//
		/**
		 * Test / deleteOne( $id, $receiver_id )
		 * 
		 */
		private function testDeleteOne() {
			//prob( $this->header_test . " / testDeleteOne()</h2>" );

			dx( $this->facade->deleteOne( 5, 1 )); //$id, $receiver_id
			dx( $this->facade->deleteOne( 9, 1 ));
		}

		/**
		 * Test / getAll( $receiver_id )
		 * 
		 */
		private function testGetAll() {
			//prob( $this->header_test . " / testGetAll()</h2>" );

			dx( $this->facade->getAll( 1 ));
		}

		/**
		 * Test / getOne( $id, $receiver_id )
		 * 
		 */
		private function testGetOne() {
			//prob( $this->header_test . " / testGetOne()</h2>" );

			dx( $this->facade->getOne( 3, 2 ));
			dx( $this->facade->getOne( 2, 2 )); //no receptor
		}

		/**
		 * Test / getUnread( $user_id )
		 * 
		 */
		private function testGetUnread() {
			//prob( $this->header_test . " / testGetUnread()</h2>" );

			dx( $this->facade->getUnread( 1 )); //$user_id
			dx( $this->facade->getUnread( 2 ));
		}

		/**
		 * Test / send( $id, $destinatary_id )
		 * 
		 */
		private function testSend() {
			//prob( $this->header_test . " / testSend()</h2>" );

			dx( $this->facade->send( 1, 4 )); //$id, $destinatary_id
			dx( $this->facade->send( 2, 4 ));
			dx( $this->facade->send( 3, 4 ));
		}

		/**
		 * Test / setRead( $id, $receiver_id )
		 * 
		 */
		private function testsetRead() {
			//prob( $this->header_test . " / testsetRead()</h2>" );

			dx( $this->facade->setRead( 1, 4 )); //$id, $receiver_id
			dx( $this->facade->setRead( 2, 4 ));
			dx( $this->facade->setRead( 3, 4 )); //no receptor
		}

} //class
