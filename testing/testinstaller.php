<?php namespace ironwoods\modules\messages\testing;

/**
 * @file: testinstaller.php
 * @info: Class for testing a process of installation for the module
 *
 * @utor: Moisés Alcocer
 * 2017, <m_alc1@hotmail.com>
 * http://www.ironwoods.es
 */

use \ironwoods\modules\messages\config\Config as Config;
use \ironwoods\modules\messages\services\Installer as Installer;


class TestInstaller {
	
	/**********************************/
	/*** Properties declaration *******/

		private $class 	= "TestInstaller";
		
		private $con = NULL;


	/**********************************/
	/*** Methods declaration **********/

		public function __construct( $n_test=NULL ) {
			//prob( $this->class . " / __construct() -> Creando conexión...<hr>" );

			$sdbcon = new \ironwoods\common\mysql\SimpleDbCon();
			$sdbcon->setDbName( "test-messages-mod" )
				->setDbUser( "root" )
				->setDbPass( "" );

			$this->con = $sdbcon->getConnection();
			$this->run( $n_test );
		}

	/*** Public Methods ***************/

		public function run( $n_test ) {
			//prob( $this->class . " / run() -> Runing Test...<br>" );

			if ( $this->con ) {

				$arr = [
					"table-prefix"	=> "test_",
					"table"			=> "owners",
					"id"			=> "id",
					"id-type"		=> "INT",
					"id-length"		=> 10,
					"id-unsigned"	=> TRUE,
				];

				/**
				 * Test 1
				 * 
				 */
				if ( $n_test === 1 ) {
					//prob( "Runs Test 1 -> Installer" );
					$installer = new Installer( $this->con, $arr );
					$installer->execute();
				}
					
				/**
				 * Test 2
				 * 
				 */
				if ( $n_test === 2 ) {
					//prob( "<hr>Runs Test 2 -> Installer" );
					$arr[ "table" ]	= "xxx"; //will produce an error
					$arr[ "id" ]	= "";
					
					$installer = new Installer( $this->con, $arr );
					$installer->execute();
				}
				/**/

			} else
				err( "Err -> Db connection don´t exist", TRUE );
		}

	/*** Private Methods **************/



} //class
