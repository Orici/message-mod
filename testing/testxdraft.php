<?php namespace ironwoods\modules\messages\testing;

/**
 * @file: testxdraft.php
 * @info: Class for testing the class XDraft / methods to create
 *
 * @utor: Moisés Alcocer
 * 2017, <m_alc1@hotmail.com>
 * http://www.ironwoods.es
 */

use \ironwoods\modules\messages\model\XDraft as XDraft;
use \ironwoods\modules\messages\entities\Message as Message;


class TestXDraft {
	
	/**********************************/
	/*** Properties declaration *******/

		private $class 	= "TestXDraft";
		private $header_test = "<h2 class='green'>Runs Test: TestXDraft";

		private $xdraft = NULL;
		private $msg  = NULL;

		private $methods_to_test = [
			"addOne",
			"changeContent",
			"changeSubject",
			"deleteOne",
			"getAll",
			"getOne",
		];


	/**********************************/
	/*** Methods declaration **********/

		public function __construct( $test_name=NULL, $test_number=NULL ) {
			////prob( $this->class . " / __construct() <hr>" );

			$sdbcon = new \ironwoods\common\mysql\SimpleDbCon();
			$sdbcon->setDbName( "test-messages-mod" )
				->setDbUser( "root" )
				->setDbPass( "" );

			$this->xdraft = new XDraft( $sdbcon->getConnection( ));
			$this->msg = new Message(
				1,
				"Mi Test",
				"Lorem ipsum ..."
			);

			$this->run( $test_name, $test_number );
		}


	/*** Public Methods ***************/

		public function run( $test_name, $test_number ) {
			////prob( $this->class . " / run() -> Runing Test...<br>" );

			if ( $this->xdraft && $this->msg ) {
				
				if ( $test_name == "addOne" )
					$this->testAddOne( $test_number );

				if ( $test_name == "changeContent" )
					$this->testChangeContent( $test_number );

				if ( $test_name == "changeSubject" )
					$this->testChangeSubject( $test_number );

				if ( $test_name == "deleteOne" )
					$this->testDeleteOne( $test_number );

				if ( $test_name == "getAll" )
					$this->testGetAll( $test_number );

				if ( $test_name == "getOne" )
					$this->testGetOne( $test_number );

				
			} else
				err( "Err -> Instance of XDraft don´t exist", TRUE );
		}

	/*** Private Methods **************/

		/**
		 * Test / addOne( $draft )
		 * 
		 */
		private function testAddOne( $n ) {
			//prob( $this->header_test . " / testAddOne()</h2>" );
			
			if ( $n === 1 ) {
				$res1 = $this->xdraft->addOne( $this->msg ); // $draft 
				
				//prob( "Crear borrador (mensaje sin destinatario)" );
				dx( $res1 );
			}
				
			if ( $n === 2 ) {
				$this->msg->setSenderId( 2 );
				$this->msg->setContent( "la, la, laaa" );
				$res2 = $this->xdraft->addOne( $this->msg );

				//prob( "Crear mensaje (mensaje con destinatario)" );
				dx( $res2 );
			}
		}

		/**
		 * Test / changeContent( $id, $str )
		 * 
		 */
		private function testChangeContent() {
			//prob( $this->header_test . " / testChangeContent()</h2>" );

			$res1 = $this->xdraft->changeContent( 1, "nuevo contenido" ); //$id, $str
			$res2 = $this->xdraft->changeContent( 3, "nuevo contenido" ); //error WHERE
			
			if ( ! $res1 )
			    err( "Err -> Actualizando registro con ID 1" );

			if ( ! $res2 ) 
			    err( "Err -> Actualizando registro con ID 3" );
		}
		
		/**
		 * Test / changeSubject( $id, $str )
		 * 
		 */
		private function testChangeSubject() {
			//prob( $this->header_test . " / testChangeSubject()</h2>" );

			$res1 = $this->xdraft->changeSubject( 1, "nuevo asunto" ); //$id, $str
			$res2 = $this->xdraft->changeSubject( 3, "nuevo asunto" ); //error WHERE

			if ( ! $res1 )
			    err( "Err -> Actualizando registro con ID 1" );

			if ( ! $res2 )
			    err( "Err -> Actualizando registro con ID 3" );
		}

		/**
		 * Test / deleteOne( $id, $writer_id )
		 * 
		 */
		private function testDeleteOne() {
			//prob( $this->header_test . " / testDeleteOne()</h2>" );

			$res1 = $this->xdraft->deleteOne( 1, 1 ); //$table, $id
			$res2 = $this->xdraft->deleteOne( 2, 1 );
			$res3 = $this->xdraft->deleteOne( 3, 1 );
			var_dump( $res1 );
			var_dump( $res2 );
			var_dump( $res3 );

			if ( ! $res1 )
			    err( "Err -> Borrando mensaje con ID 1" );

			if ( ! $res2 )
			    err( "Err -> Borrando mensaje con ID 2" );

			if ( ! $res3 )
			    err( "Err -> Borrando mensaje con ID 3" );
		}

		/**
		 * Test / getAll( $id )
		 * 
		 */
		private function testGetAll() {
			//prob( $this->header_test . " / testGetAll()</h2>" );

			dx( $this->xdraft->getAll( 1 ));
		}

		/**
		 * Test / getOne( $sender_id, $id )
		 * 
		 */
		private function testGetOne() {
			//prob( $this->header_test . " / testGetOne()</h2>" );

			//prob( "4 primeros registros del usurio ID: 1, si aun no fueron enviados:" );
			dx( $this->xdraft->getOne( 1, 1 ));
			dx( $this->xdraft->getOne( 1, 2 ));
			dx( $this->xdraft->getOne( 1, 3 ));
			dx( $this->xdraft->getOne( 1, 4 ));	
		}

} //class
