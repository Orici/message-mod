<?php namespace ironwoods\modules\messages\testing;

/**
 * @file: testxxx.php
 * @info: Class for testing ...
 *
 * @utor: Moisés Alcocer
 * 2017, <m_alc1@hotmail.com>
 * http://www.ironwoods.es
 */

use \ironwoods\modules\messages\libs\Model as Model;


class TestModel extends Model {
	
	/**********************************/
	/*** Properties declaration *******/

		private $class 	= "TestModel";

		private $sql1 = "INSERT INTO test VALUES 
			(NULL, ?, ?, CURRENT_TIMESTAMP, 0)";
		private $sql2 = "INSERT INTO test VALUES  
			(NULL, 'otro', '45', CURRENT_TIMESTAMP, 0)";

		private $arr = NULL;

	/**********************************/
	/*** Methods declaration **********/

		public function __construct( $n_test=NULL ) {
			//prob( $this->class . " / __construct() <hr>" );

			$sdbcon = new \ironwoods\common\mysql\SimpleDbCon();
			$sdbcon->setDbName( "test-messages-mod" )
				->setDbUser( "root" )
				->setDbPass( "" );


			$this->arr = [ $this->sql1, $this->sql2 ];

			$this->con = $sdbcon->getConnection();
			$this->run( $n_test );
		}

	/*** Public Methods ***************/

		public function run( $n_test ) {
			//prob( $this->class . " / run() -> Runing Test...<br>" );

			if ( $this->con ) {
				echo "<pre>";

				if ( $n_test == 1 )
					$this->test1();

				if ( $n_test == 2 )
					$this->test2();

				if ( $n_test == 3 )
					$this->test3();

				if ( $n_test == 4 )
					$this->test4();

			} else
				err( "Err -> Db connection don´t exist", TRUE );
		}

	/*** Private Methods **************/

		/**
		 * Test 1
		 * 
		 */
		private function test1() {
			//prob( "<h2 class='green'>Runs Test 1 - runQueries()</h2>" );

			$res = $this->runQueries( $this->arr, [[ "Rabocop", 32 ], []] );
			var_dump( $res ); die();
		}

		/**
		 * Test 2
		 * 
		 */
		private function test2() {
			//prob( "<h2 class='green'>Runs Test 2 - runQuery()</h2>" );

			$res = $this->runQueries( 
				[ 
					"SELECT * FROM test WHERE id > 5",
					"SELECT * FROM test WHERE nombre = 'webon'",
					"SELECT * FROM test",
				]
			);
			if ( $res ) {
				foreach ( $res as $key => $x ) {

					//prob( "Resultado consulta " . $key . ": " );
					var_dump( $x->fetchAll( ));
				}
			}
		}

		/**
		 * Test 3
		 * 
		 */
		private function test3() {
			//prob( "<h2 class='green'>Runs Test 3 - runQuery() / Binding</h2>" );


			$res = $this->runQuery( $this->sql1, [ "Rabocop", 32 ] );
			var_dump( $res );
		}

		/**
		 * Test 4
		 * 
		 */
		private function test4() {
			//prob( "<h2 class='green'>Runs Test 4 - runQuery() / Binding</h2>" );
			$sql = "UPDATE test SET nombre=? WHERE nombre=?";
			$params = [ "Pijo Pato", "Rabocop" ];
			
			$res = $this->runQuery( $sql, $params );
			var_dump( $res );
		}

} //class
