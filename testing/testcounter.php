<?php namespace ironwoods\modules\messages\testing;

/**
 * @file: testcounter.php
 * @info: Class for testing methods from the class: Count
 *
 * @utor: Moisés Alcocer
 * 2017, <m_alc1@hotmail.com>
 * http://www.ironwoods.es
 */

use \ironwoods\modules\messages\model\Counter as Counter;


class TestCounter {
	
	/**********************************/
	/*** Properties declaration *******/

		private $class 	= "TestCounter";
		
		private $counter = NULL;


	/**********************************/
	/*** Methods declaration **********/

		public function __construct( $n_test=NULL ) {
			//prob( $this->class . " / __construct() <hr>" );

			$sdbcon = new \ironwoods\common\mysql\SimpleDbCon();
			$sdbcon->setDbName( "test-messages-mod" )
				->setDbUser( "root" )
				->setDbPass( "" );

			$this->counter = new Counter( $sdbcon->getConnection( ));
			$this->run( $n_test );
		}

	/*** Public Methods ***************/

		public function run( $n_test ) {
			//prob( $this->class . " / run() -> Runing Test...<br>" );

			if ( $this->counter ) {

				if ( $n_test == 1 )
					$this->test1();

				if ( $n_test == 2 )
					$this->test2();

				if ( $n_test == 3 )
					$this->test3();


			} else
				err( "Err -> Db connection don´t exist", TRUE );
		}

	/*** Private Methods **************/

		/**
		 * Test 1
		 * 
		 */
		private function test1() {
			//prob( "<h2 class='green'>Runs Test 1 - Counter / all()</h2>" );

			$id1 = 1;
			$id2 = 2;
			$id3 = 3;
			$id4 = 4;
			$id5 = 5;
			$res1 = $this->counter->all( $id1 );
			$res2 = $this->counter->all( $id2 );
			$res3 = $this->counter->all( $id3 );
			$res4 = $this->counter->all( $id4 );
			$res5 = $this->counter->all( $id5 );

			//prob( "Mensajes usuario con ID {$id1}: " . $res1 );
			//prob( "Mensajes usuario con ID {$id2}: " . $res2 );
			//prob( "Mensajes usuario con ID {$id3}: " . $res3 );
			//prob( "Mensajes usuario con ID {$id4}: " . $res4 );
			//prob( "Mensajes usuario con ID {$id5}: " . $res5 );
		}

		/**
		 * Test 2
		 * 
		 */
		private function test2() {
			//prob( "<h2 class='green'>Runs Test 2 - Counter / unread()</h2>" );

			$id1 = 1;
			$id2 = 2;
			$id3 = 3;
			$id4 = 4;
			$id5 = 5;
			$res1 = $this->counter->unread( $id1 );
			$res2 = $this->counter->unread( $id2 );
			$res3 = $this->counter->unread( $id3 );
			$res4 = $this->counter->unread( $id4 );
			$res5 = $this->counter->unread( $id5 );

			//prob( "Mensajes usuario con ID {$id1}: " . $res1 );
			//prob( "Mensajes usuario con ID {$id2}: " . $res2 );
			//prob( "Mensajes usuario con ID {$id3}: " . $res3 );
			//prob( "Mensajes usuario con ID {$id4}: " . $res4 );
			//prob( "Mensajes usuario con ID {$id5}: " . $res5 );
		}

} //class
