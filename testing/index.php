<?php

/**
 * @file: index.php
 * @info: 
 *
 * @utor: Moisés Alcocer
 * 2017, <m_alc1@hotmail.com>
 * http://www.ironwoods.es
 */

chdir( "../" ); //cambia ejecución a directorio raiz (afecta a las rutas de los ficheros)
//echo( "Current dir. -> " . getcwd() . "<br>" ); // current directory

//requires
require "libs/requires.php";
require "entities/owner.php";

require "simpledbcon.php";
require "testconfig.php";
require "testinstaller.php";
require "testuninstaller.php";
require "testfaker.php";
require "testhelpers.php";
require "testcounter.php";
require "testxdraft.php";
require "testxmessage.php";
require "testmodel.php";

require "messages.class.php"; //main class
require "testfacade.php";


/**
 * Test
 * 
 */
print_styles();

//new ironwoods\modules\messages\testing\TestConfig( 1 );
//new ironwoods\modules\messages\testing\TestConfig( 2 );
//new ironwoods\modules\messages\testing\TestConfig( 3 ); //OK
/*new ironwoods\modules\messages\testing\TestInstaller( 1 );
new ironwoods\modules\messages\testing\TestInstaller( 2 );
new ironwoods\modules\messages\testing\TestUninstaller( 1 );
new ironwoods\modules\messages\testing\TestFaker( 1 );
new ironwoods\modules\messages\testing\TestFaker( 2 );*/
//new ironwoods\modules\messages\testing\TestFaker( 3 ); die();//renove()
//new ironwoods\modules\messages\testing\TestFaker( 4 );
new ironwoods\modules\messages\testing\TestCounter( 1 );
new ironwoods\modules\messages\testing\TestCounter( 2 );
/*
new ironwoods\modules\messages\testing\TestXMessage( "deleteOne" );
new ironwoods\modules\messages\testing\TestXMessage( "getAll" );
new ironwoods\modules\messages\testing\TestXMessage( "getOne" );
new ironwoods\modules\messages\testing\TestXMessage( "getUnread" );
new ironwoods\modules\messages\testing\TestXMessage( "send" );
new ironwoods\modules\messages\testing\TestXMessage( "setRead" );

new ironwoods\modules\messages\testing\TestXDraft( "addOne", 1 );
new ironwoods\modules\messages\testing\TestXDraft( "addOne", 2 );
/*new ironwoods\modules\messages\testing\TestXDraft( "changeContent" );
new ironwoods\modules\messages\testing\TestXDraft( "changeSubject" );
new ironwoods\modules\messages\testing\TestXDraft( "deleteOne" );
new ironwoods\modules\messages\testing\TestXDraft( "getAll" );
new ironwoods\modules\messages\testing\TestXDraft( "getOne" );
/*
new ironwoods\modules\messages\testing\TestModel( 1 );
new ironwoods\modules\messages\testing\TestModel( 2 );
new ironwoods\modules\messages\testing\TestModel( 3 );
new ironwoods\modules\messages\testing\TestModel( 4 );

new ironwoods\modules\messages\testing\TestHelpers( "ParamsValidator", 1 );
new ironwoods\modules\messages\testing\TestHelpers( "ParamsValidator", 2 );
new ironwoods\modules\messages\testing\TestHelpers( "ParamsValidator", 3 );
new ironwoods\modules\messages\testing\TestHelpers( "SqlComposer", 1 );
new ironwoods\modules\messages\testing\TestHelpers( "SqlComposer", 2 );
*/
/*
new ironwoods\modules\messages\testing\TestFacade( "install" );
new ironwoods\modules\messages\testing\TestFacade( "uninstall" );

new ironwoods\modules\messages\testing\TestFacade( "countMsgs" );
new ironwoods\modules\messages\testing\TestFacade( "countNewMsgs" );
new ironwoods\modules\messages\testing\TestFaker( 2 );
new ironwoods\modules\messages\testing\TestFacade( "countMsgs" );
new ironwoods\modules\messages\testing\TestFacade( "countNewMsgs" );

new ironwoods\modules\messages\testing\TestFacade( "addOneDraft" );
new ironwoods\modules\messages\testing\TestFacade( "changeDraftContent" );
new ironwoods\modules\messages\testing\TestFacade( "changeDraftSubject" );
*/new ironwoods\modules\messages\testing\TestFacade( "getAllDrafts" );

new ironwoods\modules\messages\testing\TestFacade( "deleteOneDraft" );/*
new ironwoods\modules\messages\testing\TestFacade( "getAllDrafts" );
new ironwoods\modules\messages\testing\TestFacade( "getOneDraft" );
//Test XMessage
*/new ironwoods\modules\messages\testing\TestFacade( "deleteOne" );
new ironwoods\modules\messages\testing\TestFacade( "getAll" );/*
new ironwoods\modules\messages\testing\TestFacade( "getOne" );
new ironwoods\modules\messages\testing\TestFacade( "getUnread" );
new ironwoods\modules\messages\testing\TestFacade( "send" );
new ironwoods\modules\messages\testing\TestFacade( "setRead" );

/**/