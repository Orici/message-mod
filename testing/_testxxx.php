<?php namespace ironwoods\modules\messages\testing;

/**
 * @file: testxxx.php
 * @info: Class for testing ...
 *
 * @utor: Moisés Alcocer
 * 2017, <m_alc1@hotmail.com>
 * http://www.ironwoods.es
 */

use \ironwoods\modules\messages\xxx\Zzz as Zzz;


class TestXxx {
	
	/**********************************/
	/*** Properties declaration *******/

		private $class 	= "TestXxx";
		
		private $con = NULL;


	/**********************************/
	/*** Methods declaration **********/

		public function __construct( $n_test=NULL ) {
			//prob( $this->class . " / __construct() <hr>" );

			$sdbcon = new \ironwoods\common\mysql\SimpleDbCon();
			$sdbcon->setDbName( "test-messages-mod" )
				->setDbUser( "root" )
				->setDbPass( "" );

			$this->con = $sdbcon->getConnection();
			$this->run( $n_test );
		}

	/*** Public Methods ***************/

		public function run( $n_test ) {
			//prob( $this->class . " / run() -> Runing Test...<br>" );

			if ( $this->con ) {

				if ( $n_test == 1 )
					$this->test1();

				if ( $n_test == 2 )
					$this->test2();

				if ( $n_test == 3 )
					$this->test3();


			} else
				err( "Err -> Db connection don´t exist", TRUE );
		}

	/*** Private Methods **************/

		/**
		 * Test 1
		 * 
		 */
		private function test1() {
			//prob( "<h2 class='green'>Runs Test 1 - xxx</h2>" );


		}

		/**
		 * Test 2
		 * 
		 */
		private function test2() {
			//prob( "<h2 class='green'>Runs Test 2 - xxx</h2>" );


		}

		/**
		 * Test 3
		 * 
		 */
		private function test3() {
			//prob( "<h2 class='green'>Runs Test 3 - xxx</h2>" );


		}

} //class
