<?php namespace ironwoods\modules\messages\testing;

/**
 * @file: testxmessage.php
 * @info: Class for testing the class XMessage methods
 *
 * @utor: Moisés Alcocer
 * 2017, <m_alc1@hotmail.com>
 * http://www.ironwoods.es
 */

use \ironwoods\modules\messages\model\XMessage as XMessage;
use \ironwoods\modules\messages\entities\Message as Message;


class TestXMessage {
	
	/**********************************/
	/*** Properties declaration *******/

		private $class 	= "TestXMessage";
		private $header_test = "<h2 class='green'>Runs Test: TestXMessage";

		private $xmsg = NULL;
		private $msg  = NULL;

		private $methods_to_test = [
			"deleteOne",
			"getAll",
			"getOne",
			"getUnread",
			"send",
			"setRead",
		];


	/**********************************/
	/*** Methods declaration **********/

		public function __construct( $test_name=NULL, $test_number=NULL ) {
			////prob( $this->class . " / __construct() <hr>" );

			$sdbcon = new \ironwoods\common\mysql\SimpleDbCon();
			$sdbcon->setDbName( "test-messages-mod" )
				->setDbUser( "root" )
				->setDbPass( "" );

			$this->xmsg = new XMessage( $sdbcon->getConnection( ));
			$this->msg = new Message(
				1,
				"Mi Test",
				"Lorem ipsum ..."
			);

			$this->run( $test_name, $test_number );
		}


	/*** Public Methods ***************/

		public function run( $test_name, $test_number ) {
			//prob( $this->class . " / run() -> Runing Test...<br>" );

			if ( $this->xmsg && $this->msg ) {
				
				switch ( $test_name ) {
					case 'deleteOne': 
						$this->testDeleteMsg( $test_number );	
						break;

					case 'getAll': 
						$this->testGetAll( $test_number );		
						break;

					case 'getOne': 
						$this->testGetOne( $test_number );		
						break;

					case 'getUnread':
						$this->testGetUnread( $test_number );
						break;

					case 'send':
						$this->testSend( $test_number );
						break;

					case 'setRead':
						$this->testsetRead( $test_number );
						break;
				}


			} else
				err( "Err -> Instance of XDraft don´t exist", TRUE );
		}

	/*** Private Methods **************/

		/**
		 * Test -> deleteOne( $id, $receiver_id )
		 * 
		 */
		private function testDeleteMsg( $test_number ) {
			//prob( $this->header_test . " / testDeleteMsg()</h2>" );

			$res1 = $this->xmsg->deleteOne( 1, 1 ); //$id, $receiver_id
			$res2 = $this->xmsg->deleteOne( 3, 1 );
			var_dump( $res1 );
			var_dump( $res2 );

			if ( ! $res1 )
			    err( "Err -> Borrando mensaje con ID: 1 / Usuario ID: 1" );

			if ( ! $res2 )
			    err( "Err -> Borrando mensaje con ID: 3 / Usuario ID: 1" );
		}

		/**
		 * Test / getAll( $receiver_id )
		 * 
		 */
		private function testGetAll( $test_number ) {
			//prob( $this->header_test . " /  getAll()</h2>" );

			dx( $this->xmsg->getAll( 1 ));
			dx( $this->xmsg->getAll( 2 ));
		}

		/**
		 * Test / getOne( $id, $receiver_id )
		 * 
		 */
		private function testGetOne( $test_number ) {
			//prob( $this->header_test . " /  getOne()</h2>" );

			dx( $this->xmsg->getOne( 1, 1 ));
			dx( $this->xmsg->getOne( 5, 1 ));	
		}

		/**
		 * Test / getUnread( $user_id )
		 * 
		 */
		private function testGetUnread( $test_number ) {
			//prob( $this->header_test . " /  getUnread()</h2>" );

			dx( $this->xmsg->getUnread( 1 ));
			dx( $this->xmsg->getUnread( 4 ));
			dx( $this->xmsg->getUnread( 8 )); //usuario inexistente
		}

		/**
		 * Test / send( $id, $destinatary_id )
		 * 
		 */
		private function testSend( $test_number ) {
			//prob( $this->header_test . " /  send()</h2>" );

			$res1 = $this->xmsg->send( 1, 2 ); //$id, $id_destinatary
			$res2 = $this->xmsg->send( 3, 2 ); //error WHERE

			if ( ! $res1 )
			    err( "Err -> Enviando mensaje con ID 1" ); // Actualizando registro

			if ( ! $res2 ) 
			    err( "Err -> Enviando mensaje con ID 3" ); // Actualizando registro
		}

		/**
		 * Test / setRead( $id_msg )
		 * 
		 */
		private function testsetRead( $test_number ) {
			//prob( $this->header_test . " /  setRead()</h2>" );

			$res1 = $this->xmsg->setRead( 1 ); //$id
			$res2 = $this->xmsg->setRead( 3 ); //error WHERE

			if ( ! $res1 )
			    err( "Err -> Actualizando registro con ID 1" );

			if ( ! $res2 ) 
			    err( "Err -> Actualizando registro con ID 3" );
		}

} //class
