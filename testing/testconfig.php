<?php namespace ironwoods\modules\messages\testing;

/**
 * @file: testconfig.php
 * @info: Class for testing the class "Config"
 *
 * @utor: Moisés Alcocer
 * 2017, <m_alc1@hotmail.com>
 * http://www.ironwoods.es
 */


use \ironwoods\modules\messages\config\Config as Config;

class TestConfig {
	
	/**********************************/
	/*** Properties declaration *******/

		private $class 	= "TestConfig";

		private $config = NULL;


	/**********************************/
	/*** Methods declaration **********/

		public function __construct( $n_test=NULL ) {
			//prob( $this->class . " / __construct()" );

			$this->config = new Config();
			$this->run( $n_test );
		}

	/*** Public Methods ***************/

		public function run( $n_test ) {
			//prob( $this->class . " / run() -> Runing Test...<br>" );
			echo "<pre>";
			
			$this->setFile( $n_test );
			//prob( "<br><hr><hr>" );
			$this->getData( $n_test );
			//prob( "<br><hr><hr>" );
			$this->delFile( $n_test );
		}

	/*** Private Methods **************/

		private function delFile( $n_test ) {

			if ( $n_test == 1 ) {
				//prob( "<h2 class='green'>Runs Test 1 -> Config / delFile()</h2>" );
				$res_a = $this->config->delFile();
				var_dump( $res_a );
			}
		}

		private function getData( $n_test ) {

			if ( $n_test == 1 ) {
				//prob( "<h2 class='green'>Runs Test 1 -> Config / getData()</h2>" );
				$arr_a = $this->config->getData();
				var_dump( $arr_a );
			}	
		}

		private function setFile( $n_test ) {

			$arr_data = array();
			$arr_data[ "table-prefix" ] = "";
			$arr_data[ "table" ] 		= "test-a";
			$arr_data[ "id" ] 			= "id";
			$arr_data[ "id-type" ] 		= "INT";
			$arr_data[ "id-length" ] 	= 10;
			$arr_data[ "id-unsigned" ] 	= TRUE;

			if ( $n_test == 1 ) {
				//prob( "<h2 class='green'>Runs Test 1 -> Config / setFile()</h2>" );

				$this->config->setFile( $arr_data );
			}

			if ( $n_test == 2 ) {
				//prob( "<h2 class='green'>Runs Test 2 -> Config / setFile()</h2>" );

				$arr_data[ "table-prefix" ] = "b-"; //erroneo para crear tabla
				$arr_data[ "table" ] 		= "bbb";
				$arr_data[ "id" ] 			= "id_table_test_b";
				$arr_data[ "id-unsigned" ] 	= FALSE;
				
				$this->config->setFile( $arr_data );
			}
			
			if ( $n_test == 3 ) {
				//prob( "<h2 class='green'>Runs Test 3 -> Config / setFile()</h2>" );

				$arr_data[ "table-prefix" ] = "test_";
				$arr_data[ "table" ] 		= "table_CCC";
				$arr_data[ "id" ] 			= "";
				$arr_data[ "id-type" ] 		= "TINYINT";
				$arr_data[ "id-length" ] 	= 4;
				
				$this->config->setFile( $arr_data );
			}
		}

} //class
