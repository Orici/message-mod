<?php namespace ironwoods\modules\messages\testing;

/**
 * @file: testuninstaller.php
 * @info: Class for testing a process of desinstallation for the module
 *
 * @utor: Moisés Alcocer
 * 2017, <m_alc1@hotmail.com>
 * http://www.ironwoods.es
 */

use \ironwoods\modules\messages\config\Config as Config;
use \ironwoods\modules\messages\services\Uninstaller as Uninstaller;


class TestUninstaller {
	
	/**********************************/
	/*** Properties declaration *******/

		private $class 	= "TestUninstaller";
		
		private $con = NULL;


	/**********************************/
	/*** Methods declaration **********/

		public function __construct( $n_test=NULL ) {
			//prob( $this->class . " / __construct() -> Creando conexión...<hr>" );

			$sdbcon = new \ironwoods\common\mysql\SimpleDbCon();
			$sdbcon->setDbName( "test-messages-mod" )
				->setDbUser( "root" )
				->setDbPass( "" );

			$this->con = $sdbcon->getConnection();
			$this->run( $n_test );
		}

	/*** Public Methods ***************/

		public function run( $n_test ) {
			//prob( $this->class . " / run() -> Runing Test...<br>" );

			if ( $this->con ) {

				/**
				 * Test 1
				 *
				 */
				if ( $n_test == 1 ) {
					//prob( "Runs Test 1 -> " . $this->class );
					$x = new Uninstaller( $this->con );
					$x->execute();
				}

			} else
				err( "Err -> Db connection don´t exist", TRUE );
		}

	/*** Private Methods **************/



} //class
