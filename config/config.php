<?php namespace ironwoods\modules\messages\config;

/**
 * @file: config.php
 * @info: Class to handle operations over configuration
 *
 * @utor: Moisés Alcocer
 * 2017, <m_alc1@hotmail.com>
 * http://www.ironwoods.es
 */

use \ironwoods\modules\messages\helpers\Files as Files;
use \ironwoods\modules\messages\helpers\ParamsValidator as PVal;


class Config {
	
	/**********************************/
	/*** Properties declaration *******/

		private $class = "Config";

		private $file_for_ext_config = "config/files/config_ext.php";


	/**********************************/
	/*** Methods declaration **********/

	/*** Public Methods ***************/

		/**
		 * Gets settings data
		 *
		 * @return     array
		 */
		public function getData() {
			//prob( $this->class . " / getData()" );

			$path = $this->file_for_ext_config;


			if ( file_exists( $path ))
				return require( $path );

			err( "Err -> Isn´t possible to return the data", TRUE );
		}

		/**
		 * Gets the file path for file settings
		 *
		 * @return     string
		 */
		public function getFilePath() {
			
			return $this->file_for_ext_config;
		}

		/**
		 * Deletes the file from the HD
		 *
		 * @return		boolean
		 */
		public function delFile() {
			//prob( $this->class . " / delFile()" );
			

			return unlink( $this->file_for_ext_config );
		}

		/**
		 * Creates the file in the HD
		 *
		 * @param		array 		$arr_data
		 * @return		boolean
		 */
		public function setFile( $arr_data=NULL ) {
			//prob( $this->class . " / setFile()" );

			if ( PVal::settingsData( $arr_data )) {

				return $this->writeConfigFile( //implementar función
					$arr_data[ "table-prefix" ],
					$arr_data[ "table" ],
					$arr_data[ "id" ],
					$arr_data[ "id-type" ],
					$arr_data[ "id-length" ],
					$arr_data[ "id-unsigned" ]
				);
			}
			//prob( $this->class . " / setFile() -> NO pasa PVal..." );


			return FALSE;
		}


	/*** Private Methods **************/

		/**
		 * Composes the content and writes the configuration file
		 *
		 * @param	  string  	$table_prefix
		 * @param	  string  	$table
		 * @param	  string  	$id
		 * @param	  string  	$id_type
		 * @param	  int	  	$id_length
		 * @param	  boolean  	$id_unsigned
		 * @return    boolean
		 */
		private function writeConfigFile( 
			$table_prefix,
			$table, 
			$id, 
			$id_type,
			$id_length,
			$id_unsigned
		) {
			//prob( $this->class . " / writeConfigFile()" );

			$table_prefix	= ( $table_prefix )	? $table_prefix : "";
			$id_unsigned	= ( $id_unsigned )	? "TRUE" : "FALSE";
			$id = ( $id ) ? $id : "id";
			
			$str = "<?php"	. PHP_EOL . PHP_EOL;
			$str .= "/**"	. PHP_EOL;
			$str .= " * Settings file for messages module" . PHP_EOL;
			$str .= " * "	. PHP_EOL;
			$str .= " * Moisés Alcocer, 2017" . PHP_EOL;
			$str .= " * http://www.ironwoods.es" . PHP_EOL;
			$str .= " * "	. PHP_EOL;
			$str .= " */"	. PHP_EOL;	
			$str .= PHP_EOL . PHP_EOL;
			
			$str .= "return [" . PHP_EOL;
			$str .= "\t\"table-prefix\"	=> \"" . $table_prefix	. "\"," . PHP_EOL;
			$str .= "\t\"table\" 		=> \"" . $table			. "\"," . PHP_EOL;
			$str .= "\t\"id\"			=> \"" . $id 			. "\"," . PHP_EOL;
			$str .= "\t\"id-type\"		=> \"" . $id_type		. "\"," . PHP_EOL;
			$str .= "\t\"id-length\" 	=> "   . $id_length		. ","   . PHP_EOL;
			$str .= "\t\"id-unsigned\"	=> \"" . $id_unsigned	. "\"," . PHP_EOL;
			$str .= "];" . PHP_EOL;

			////prob( "Actual directory: " . getcwd( ) . "<hr>" );
			return Files::write( $this->file_for_ext_config, $str );
		}

} //class