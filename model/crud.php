<?php namespace ironwoods\modules\messages\model;

/**
 * @file: crud.php
 * @info: Class that implements a CRUD
 *
 * @utor: Moisés Alcocer
 * 2017, <m_alc1@hotmail.com>
 * http://www.ironwoods.es
 */

use \ironwoods\modules\messages\config\Config as Config;
use \ironwoods\modules\messages\helpers\DBValidator as DbVal;
use \ironwoods\modules\messages\helpers\ParamsValidator as PVal;
use \ironwoods\modules\messages\helpers\SqlComposer as SqlComposer;


class CRUD extends \ironwoods\modules\messages\libs\Model {
	
	/**********************************/
	/*** Properties declaration *******/

		private $class 	= "CRUD";


	/**********************************/
	/*** Methods declaration **********/

		/**
		 * Construct
		 *
		 * @param	Connection	$con	The connection with the DB
		 */
		public function __construct( $con=NULL ) {
			parent::__construct( $con );

		}

	/*** Public Methods ***************/

	////////////////////////////////////////////////////////////////////////////
	// CREATE
	//


	////////////////////////////////////////////////////////////////////////////
	// READ
	//
	

	////////////////////////////////////////////////////////////////////////////
	// UPDATE
	// 


	////////////////////////////////////////////////////////////////////////////
	// DELETE
	//



	/*** Private Methods **************/


} //class
