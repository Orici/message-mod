<?php namespace ironwoods\modules\messages\model;

/**
 * @file: xdraft.php
 * @info: Class that implements a CRUD
 *
 * @utor: Moisés Alcocer
 * 2017, <m_alc1@hotmail.com>
 * http://www.ironwoods.es
 */

use \ironwoods\modules\messages\config\Config as Config;
use \ironwoods\modules\messages\entities\Message as Message;
use \ironwoods\modules\messages\helpers\DBValidator as DbVal;
use \ironwoods\modules\messages\helpers\ParamsValidator as PVal;
use \ironwoods\modules\messages\helpers\SqlComposer as SqlComposer;


class XDraft extends \ironwoods\modules\messages\libs\Model {
	
	/**********************************/
	/*** Properties declaration *******/

		private $class 	= "XDraft";


	/**********************************/
	/*** Methods declaration **********/

		/**
		 * Construct
		 *
		 * @param	Connection	$con	The connection with the DB
		 */
		public function __construct( $con=NULL ) {
			parent::__construct( $con );

		}

	/*** Public Methods ***************/

	////////////////////////////////////////////////////////////////////////////
	// CREATE
	//
		/**
		 * Adds a new draft to table messages
		 *
		 * @param 	Message		$draft
		 * @return	int
		 */
		public function addOne( Message $draft ) {
			//prob( $this->class . " / addOne()" );
			$id = FALSE;

			if ( PVal::message( $draft )) {
				//prob( "Tabla ---> " . $this->table );

				//Loads the table from model / settings
				$sql 	= SqlComposer::insertMsgWithParams( $this->table, $draft );
				$params = SqlComposer::getParams( $draft );
				$id 	= $this->runQuery( $sql, $params );

			} else
				err( $this->class . " / addOne() -> Err args" );


			return $id;
		}


	////////////////////////////////////////////////////////////////////////////
	// READ
	//

		/**
		 * Gets the drafts for the user with the identifier
		 *
		 * @param 	int  		$writer_id
		 * @return	array
		 */
		public function getAll( $writer_id ) {
			//prob( $this->class . " / getAll()" );

			if ( PVal::id( $writer_id )) {
				
				$sql = "SELECT * FROM {$this->table} 
					WHERE sender={$writer_id}
					AND receiver IS NULL						/* Sin receptor */
					AND deleted_at = '0000-00-00 00:00:00'";	/* No eliminado */
				$res = $this->runQuery( $sql );


				if ( $res )
					return $res->fetchAll();

			} else
				err( $this->class . " / getAll() -> Err args" );

			
			return FALSE;
		}

		/**
		 * Gets the draft with the ID of the author
		 *
		 * @param   int			$id
		 * @param   int			$writer_id
		 * @return	array
		 */
		public function getOne( $id, $writer_id ) {
			//prob( $this->class . " / getOne()" );

			if ( PVal::id( $id ) && PVal::id( $writer_id )) {

				$sql = "SELECT * FROM {$this->table} 
					WHERE {$this->id}={$id}
					AND sender={$writer_id}";
				$res = $this->runQuery( $sql );


				if ( $res )
					return $res->fetch();

			} else
				err( $this->class . " / getOne() -> Err args" );


			return FALSE;
		}


	////////////////////////////////////////////////////////////////////////////
	// UPDATE
	// 
	// Is posible to change:
	//  - subject
	//  - content

		/**
		 * Changes the content for the message (only draft)
		 *
		 * @param      int		$id
		 * @param      string	$str
		 * @return     boolean
		 */
		public function changeContent( $id, $str ) {
			//prob( $this->class . " / changeContent()" );

			if ( PVal::id( $id )) {
			
				$sql = "UPDATE {$this->table} 
					SET content=?
					WHERE id=? 
					AND receiver IS NULL";
				$params = [
					$str,
					$id
				];

				return $this->runQuery( $sql, $params );

			} else
				err( $this->class . " / changeContent() -> Err args" );

			
			return FALSE;
		}

		/**
		 * Changes the subject for the message (only draft)
		 *
		 * @param      int		$id
		 * @param      string	$str
		 * @return     boolean
		 */
		public function changeSubject( $id, $str ) {
			//prob( $this->class . " / changeSubject()" );

			if ( PVal::id( $id )) {
			
				$sql = "UPDATE {$this->table} 
					SET subject=?
					WHERE id=? 
					AND receiver IS NULL";
				$params = [
					$str,
					$id
				];

				return $this->runQuery( $sql, $params );

			} else
				err( $this->class . " / changeSubject() -> Err args" );

			
			return FALSE;
		}


	////////////////////////////////////////////////////////////////////////////
	// DELETE
	//
		/**
		 * Deletes a draft by the author
		 *
		 * @param 	int			$id
		 * @param 	int  		$writer_id
		 * @return	boolean
		 */
		public function deleteOne( $id, $writer_id ) {
			//prob( $this->class . " / deleteOne()" );

			if ( PVal::id( $id ) && PVal::id( $writer_id )) {

				$sql = "UPDATE {$this->table} 
					SET deleted_at = now()
					WHERE {$this->id}={$id}
					AND sender={$writer_id}";

				return $this->runQuery( $sql );

			} else
				err( $this->class . " / deleteOne() -> Err args" );


			return FALSE;
		}


	/*** Private Methods **************/


} //class
