<?php namespace ironwoods\modules\messages\model;

/**
 * @file: xmessage.php
 * @info: Class that implements a CRUD
 *
 * @utor: Moisés Alcocer
 * 2017, <m_alc1@hotmail.com>
 * http://www.ironwoods.es
 */

use \ironwoods\modules\messages\config\Config as Config;
use \ironwoods\modules\messages\helpers\DBValidator as DbVal;
use \ironwoods\modules\messages\helpers\ParamsValidator as PVal;
use \ironwoods\modules\messages\helpers\SqlComposer as SqlComposer;


class XMessage extends \ironwoods\modules\messages\libs\Model {
	
	/**********************************/
	/*** Properties declaration *******/

		private $class 	= "XMessage";


	/**********************************/
	/*** Methods declaration **********/

		/**
		 * Construct
		 *
		 * @param	Connection	$con	The connection with the DB
		 */
		public function __construct( $con=NULL ) {
			parent::__construct( $con );

		}

	/*** Public Methods ***************/

	////////////////////////////////////////////////////////////////////////////
	// CREATE
	//
	
	////////////////////////////////////////////////////////////////////////////
	// READ
	//
		
		/**
		 * Gets the messages for the user with the identifier
		 *
		 * @param 	int  		$id -> receptor ID
		 * @return	array
		 */
		public function getAll( $id ) {
			//prob( $this->class . " / getAll()" );

			if ( PVal::id( $id )) {
				
				$sql = "SELECT * FROM {$this->table}
					WHERE receiver = {$id}
					AND deleted_at = '0000-00-00 00:00:00'";	/* No eliminado */
				$res = $this->runQuery( $sql );


				if ( $res )
					return $res->fetchAll();

			} else
				err( $this->class . " / getAll() -> Err args" );

			
			return FALSE;
		}

		/**
		 * Gets the messages for the user with the identifier
		 *
		 * @param 	int			$id
		 * @param 	int  		$receiver_id
		 * @return	array
		 */
		public function getOne( $id, $receiver_id ) {
			//prob( $this->class . " / getOne()" );

			if ( PVal::id( $id ) && PVal::id( $receiver_id )) {

				$sql = "SELECT * FROM {$this->table} 
					WHERE {$this->id} = {$id}
					AND receiver={$receiver_id}";
					//AND deleted_at = '0000-00-00 00:00:00'";	/* No eliminado */
				$res = $this->runQuery( $sql );


				if ( $res )
					return $res->fetch();

			} else
				err( $this->class . " / getOne() -> Err args" );


			return FALSE;
		}
			
		/**
		 * Gets the unread messages of the receiver
		 *
		 * @param   id			$user_id
		 * @return	array
		 */
		public function getUnread( $user_id ) {
			//prob( $this->class . " / getUnread()" );

			if ( PVal::id( $user_id )) {
				
				$sql = "SELECT * FROM {$this->table} 
					WHERE receiver = {$user_id} 
					AND read     = 0
					AND deleted_at = '0000-00-00 00:00:00'";	/* No eliminado */
				$res = $this->runQuery( $sql );


				if ( $res )
					return $res->fetchAll();

			} else
				err( $this->class . " / getUnread() -> Err args" );

			
			return FALSE;
		}


	////////////////////////////////////////////////////////////////////////////
	// UPDATE
	// 
	// Is posible to change:
	//  - receiber (if send the message)
	//  - read (only received messages)

		/**
		 * Sets the message sended ( put the ID for the receiver )
		 *
		 * @param      int		$id
		 * @param      int		$destinatary_id
		 * @return     boolean
		 */
		public function send( $id, $destinatary_id ) {
			//prob( $this->class . " / send()" );

			if ( PVal::id( $id ) && PVal::id( $destinatary_id )) {
			
				$sql = "UPDATE {$this->table} 
					SET receiver=? 
					WHERE id=?
					AND receiver IS NULL";
				$params = [
					$destinatary_id,
					$id
				];

				return $this->runQuery( $sql, $params );

			} else
				err( $this->class . " / send() -> Err args" );

			
			return FALSE;
		}

		/**
		 * Sets the field read to 1 in the table
		 *
		 * @param      int  	$id
		 * @param      int  	$receiver_id
		 * @return     boolean
		 */
		public function setRead( $id, $receiver_id ) {
			//prob( $this->class . " / setRead()" );

			if ( PVal::id( $id ) && PVal::id( $receiver_id )) {
			
				$sql = "UPDATE {$this->table} 
					SET read=1 
					WHERE id=?
					AND receiver=?";


				return $this->runQuery( $sql, [ $id, $receiver_id ] );

			} else
				err( $this->class . " / setRead() -> Err args" );

			
			return FALSE;
		}


	////////////////////////////////////////////////////////////////////////////
	// DELETE
	//

		/**
		 * Deletes a message by receiver
		 *
		 * @param 	int			$id
		 * @param 	int  		$receiver_id
		 * @return  boolean
		 */
		public function deleteOne( $id, $receiver_id ) {
			//prob( $this->class . " / deleteOne()" );

			if ( PVal::id( $id ) && PVal::id( $receiver_id )) {

				$sql = "UPDATE {$this->table}
					SET deleted_at = now()
					WHERE {$this->id}={$id} 
					AND receiver={$receiver_id}";
				
				
				return $this->runQuery( $sql );

			} else
				err( $this->class . " / deleteOne() -> Err args" );


			return FALSE;
		}


	/*** Private Methods **************/


} //class
