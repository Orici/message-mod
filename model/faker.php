<?php namespace ironwoods\modules\messages\model;

/**
 * @file: faker.php
 * @info: Class to crete fake registers in the Db for testing
 *
 * @utor: Moisés Alcocer
 * 2017, <m_alc1@hotmail.com>
 * http://www.ironwoods.es
 */

use \ironwoods\modules\messages\helpers\DBValidator as DBVal;
use \ironwoods\modules\messages\helpers\Files as Files;
use \ironwoods\modules\messages\helpers\ParamsValidator as PVal;
use \ironwoods\modules\messages\helpers\SqlComposer as SqlComposer;


class Faker
	extends \ironwoods\modules\messages\libs\Model {
	
	/**********************************/
	/*** Properties declaration *******/

		private $class 	= "Faker";


	/**********************************/
	/*** Methods declaration **********/

		/**
		 * Construct
		 *
		 * @param	Connection	$con	The connection with the DB
		 */
		public function __construct( $con=NULL ) {
			parent::__construct( $con );
		}

	/*** Public Methods ***************/

		/**
		 * Creates the tables: test_owners and test_messages
		 * If the tables exists, they are deleted and created again
		 * 
		 * @return		boolean
		 */
		public function prepareDb() {
			//prob( $this->class . " / prepareDb()" );

			//prob( "Eliminando tablas..." );
			$arr = [
				"DROP TABLE test_messages",
				"DROP TABLE test_owners",
			];
			if ( ! $this->runQueries( $arr ))
				return FALSE;

			//prob( "Creando tablas..." );
			return ( 
				$this->createTable( "owners" ) &&
				$this->createTable( "messages" )
			);
		}

		/**
		 * Adds a row in a table
		 * 
		 * @param		object		$entity
		 * @return	 	boolean
		 */
		public function put( $entity=NULL ) {
			//prob( $this->class . " / put()" );

			if ( $entity && is_object( $entity )) {
				$sql = NULL;
				//var_dump( $entity );
				
				if ( PVal::isMessage( $entity ))
					$sql = SqlComposer::insertMsg( "test_messages", $entity );

				if ( PVal::isOwner( $entity ))
					$sql = SqlComposer::insertOwner( "test_owners", $entity );

				if ( $sql ) {
					if ( ! $this->runQuery( $sql ))
						err( "Err: insertion failed !" );

				} else
					err( "Err: SQL is empty !" );
			} else
				err( $this->class . " / put() -> Err args" );
		}

		/**
		 * Cleans the tables and reseeds
		 *
		 * @param		string		$prefix_table
		 * @param		string		$ref_table
		 */
		public function renove( $prefix_table="test_", $ref_table="owners" ) {
			//prob( $this->class . " / renove()" );

			//Clean the tables
			$this->prepareDb();
			
			$this->seed( $prefix_table, $ref_table );
		}

		/**
		 * Seeds the table with fake messages for testing the module
		 * If the table has registers the news are added
		 *
		 * @param		string		$prefix_table
		 * @param 		string		$ref_table
		 * @return	 	boolean
		 */
		public function seed( $prefix_table="test_", $ref_table="owners" ) {
			//prob( $this->class . " / seed()" );

			if ( ! DBVal::existTable( $this->con, $prefix_table . "messages" )) {

				$this->createTable( $prefix_table . "messages" );

				if ( ! DBVal::existTable( $this->con, $prefix_table . $ref_table ))
					$this->createTable( $prefix_table . $ref_table );
			}

			$arr = $this->getInsertsToFakeMessages();
			
			return (
				$this->runQuery( $arr[ 0 ] ) &&
				$this->runQuery( $arr[ 1 ] )
			);
		}


	/*** Private Methods **************/

		/**
		 * Creates the tables in the DB
		 * 
		 * @param      string		$table
		 */
		private function createTable( $table ) {
			//prob( $this->class . " / createTable() -> " . $table );

			$path_create_messages = "sql/create_table_messages.sql";
			$path_create_owners   = "sql/create_table_owners.sql";
			$sql = "";

			if ( strpos( $table, "owners" ) || $table == "owners" ) {
				//prob( "Creando tabla <b class='green'>owners</b>..." );

				$sql = Files::getContent( $path_create_owners );

				return $this->runQuery( $sql );
			}
				
			if ( strpos( $table, "messages" ) || $table == "messages" ) {
				//prob( "Creando tabla <b class='green'>messages</b>..." );

				$sql = Files::getContent( $path_create_messages );
				$sql = str_replace( "messages", "test_messages", $sql );
				$sql = str_replace( "(12)", "(10) UNSIGNED", $sql );
				$sql = str_replace( "xxx", "test_owners", $sql );

				return $this->runQuery( $sql );
			}
			//prob( $this->class . " / createTable() -> Err args" );


			return FALSE;
		}

		/**
		 * Reads a file with messages for testing and returns their
		 *
		 * @return	 array
		 */
		private function getInsertsToFakeMessages() {
			//prob( $this->class . " / getInsertsToFakeMessages()" );

			$inserts_owners  = Files::getContent( "sql/inserts_owners.sql" );
			$insert_messages = Files::getContent( "sql/inserts_messages.sql" );
			////prob( $insert_messages );

			return [ $inserts_owners, $insert_messages ];
		}

} //class