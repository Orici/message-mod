<?php namespace ironwoods\modules\messages\model;

/**
 * @file: counter.php
 * @info: Class that implements count operations
 *
 * @utor: Moisés Alcocer
 * 2017, <m_alc1@hotmail.com>
 * http://www.ironwoods.es
 */

use \ironwoods\modules\messages\helpers\ParamsValidator as PVal;


class Counter
	extends \ironwoods\modules\messages\libs\Model {
	
	/**********************************/
	/*** Properties declaration *******/

		private $class 	= "Counter";


	/**********************************/
	/*** Methods declaration **********/

		/**
		 * Construct
		 *
		 * @param	Connection	$con	The connection with the DB
		 */
		public function __construct( $con=NULL ) {
			parent::__construct( $con );

		}

	/*** Public Methods ***************/

		/**
		 * Counters all received messages 
		 *
		 * @param 		int		$receiver_id
		 * @return		int
		 */
		public function all( $receiver_id ) {
			//prob( $this->class . " / all()" );

			if ( PVal::id( $receiver_id )) {
				$sql = "SELECT COUNT(*) AS count 
					FROM {$this->table} 
					WHERE receiver={$receiver_id}
					AND deleted_at = '0000-00-00 00:00:00'";

				$res = $this->runQuery( $sql );
				

				return $res->fetch()->count;

			} else
				err( $this->class . " / all() -> Err args" );
			

			return FALSE;
		}

		/**
		 * Counters unread messages 
		 *
		 * @param 		int		$receiver_id
		 * @return		int
		 */
		public function unread( $receiver_id ) {
			//prob( $this->class . " / unread()" );

			if ( PVal::id( $receiver_id )) {
				$sql = "SELECT COUNT(*) AS count 
					FROM {$this->table} 
					WHERE receiver={$receiver_id}
					AND	read = 0
					AND deleted_at = '0000-00-00 00:00:00'";

				$res = $this->runQuery( $sql );
				
				
				return $res->fetch()->count;

			} else
				err( $this->class . " / unread() -> Err args" );
			

			return FALSE;
		}


	/*** Private Methods **************/



} //class