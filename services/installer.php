<?php namespace ironwoods\modules\messages\services;

/**
 * @file: installer.php
 * @info: Class to install the module in the App
 *
 * @utor: Moisés Alcocer
 * 2017, <m_alc1@hotmail.com>
 * http://www.ironwoods.es
 */

use \ironwoods\modules\messages\config\Config as Config;
use \ironwoods\modules\messages\helpers\Files as Files;
use \ironwoods\modules\messages\helpers\DBValidator as DBVal;
use \ironwoods\modules\messages\helpers\ParamsValidator as PVal;


class Installer
	extends \ironwoods\modules\messages\libs\Model {

	/**********************************/
	/*** Properties declaration *******/

		private $class = "Installer";

		private $arr_data	= NULL;


	/**********************************/
	/*** Methods declaration **********/

		/**
		 * Construct
		 *
		 * $arr_data = [
		 * 		"table-prefix"	=> "xxx",
		 * 		"table"			=> "xxx",
		 * 		"id"			=> "xxx"
		 * 		"id-type" 		=> "xxx",
		 *		"id-length" 	=> 10,
		 *		"id-unsigned"	=> TRUE,
		 * ]
		 * @param	Object	$db_con
		 * @param	Array	$arr_data
		 */
		public function __construct( $db_con=NULL, $arr_data=NULL ) {
			parent::__construct( $db_con, $arr_data );

			if ( PVal::settingsData( $arr_data )) {
				$this->arr_data	 = $arr_data;
				
			} else
				err( "Err -> Array data for Settings is invalid", TRUE );

		}


	/*** Public Methods ***************/

		/**
		 * Runs the service
		 * 
		 */
		public function execute() {
			//prob( $this->class . " / execute()" );
			
			//Data for the installation
			
			//Creates config file
			$config = new Config();
			$file_path = $config->getFilePath();

			$config->setFile( $this->arr_data );


			//Settings file has been created -> add table to the DB
			if ( file_exists( $file_path )) {
				$table = $this->arr_data[ "table-prefix" ] 
					. $this->arr_data[ "table" ];

				//if ( $this->existTable( $table ))
					$this->addTableToDb();

				//else
				//	err( "Err: Table \"{$table}\" isn´t in DB", TRUE );
			} else
				err( "Err: \"Settings file\" don´t exists", TRUE );
		}


	/*** Private Methods **************/

		private function addTableToDb() {
			//prob( $this->class . " / addTableToDb()" );
			
			
			$sql  = $this->getSqlToCreateTable();

			if ( $this->runQuery( $sql ) && $this->existTable( 
				$this->arr_data[ "table-prefix" ] . "messages" 
			))
				//prob( $this->class . " / addTableToDb() -> Tabla creada" );

			else
				err( "Err: table don´t created" );
		}

		private function getSqlToCreateTable() {

			$path 	= "sql/create_table_messages.sql";
			$prefix = $this->arr_data[ "table-prefix" ];
			$str 	= Files::getContent( $path );

			$table 	= $prefix . $this->arr_data[ "table" ];
			$id		= $this->arr_data[ "id" ];
			$id_type	= $this->arr_data[ "id-type" ];
			$id_length	= $this->arr_data[ "id-length" ];
			$id_unsigned = $this->arr_data[ "id-unsigned" ];


			//Complete the code with the data from the system
			if ( $id )
				$str = str_replace( "xxx( id )", "xxx( {$id} )", $str );
			
			if ( $id_unsigned == "TRUE" )
				$str = str_replace( "(12) ", "(12) UNSIGNED ", $str );

			$str = str_replace( "messages(", "{$prefix}messages(", $str );
			$str = str_replace( "xxx(", "{$table}(", $str );
			$str = str_replace( "Int(12", "{$id_type}(12", $str );
			$str = str_replace( "12", "{$id_length}", $str );
			

			return $str;
		}

		/**
		 * Searchs for the table in the DB
		 *
		 * @param      string	$table
		 * @return     boolean
		 */
		private function existTable( $table=NULL ) {

			return DBVal::existTable( 
				$this->con, 
				$table 
			);
		}
			
} //class