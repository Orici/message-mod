<?php namespace ironwoods\modules\messages\services;

/**
 * @file: uninstaller.php
 * @info: Class to install the module in the App
 *
 * @utor: Moisés Alcocer
 * 2017, <m_alc1@hotmail.com>
 * http://www.ironwoods.es
 */

use \ironwoods\modules\messages\config\Config as Config;


class Uninstaller
	extends \ironwoods\modules\messages\libs\Model {

	/**********************************/
	/*** Properties declaration *******/

			private $class = "Uninstaller";
			private $path  = NULL;


	/**********************************/
	/*** Methods declaration **********/

		/**
		 * Construct
		 *
		 * @param      Object	$con
		 * @param      string	$path_config_file
		 */
		public function __construct( $con=NULL, $path_config_file=NULL ) {
			
			$this->con  = $con;
			$this->path = $path_config_file;
		}


	/*** Public Methods ***************/

		public function execute() {
			//prob( $this->class . " / execute()" );
			//prob( "Config file: " . $this->path );

			$x = new Config();
			$arr_data = $x->getData( $this->path );
			$table = $arr_data[ "table-prefix" ] . "messages";

			// Deletes file with settings
			if ( $x->delFile( $this->path )) {
				//prob( $this->class . " / execute() -> Deleting table from DB..." );
				
				// Deletes the table messages from DB
				$sql = "DROP TABLE {$table}";
				if ( ! $this->runQuery( $sql ))
					err( "Err -> The table {$table} hasn´t been desinstalled !" );
				
			} else
				err( "Err -> The file settings hasn´t been deleted !" );
		}


	/*** Private Methods **************/



} //class