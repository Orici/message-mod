<?php namespace ironwoods\modules\messages;

/**
 * @file: messages.class.php
 * @info: Main class for the module
 * 
 *  This module add to a system the messagerie internal service.
 *	This class must be required for module use.
 *	
 *	The public methods, in this class, provide all functionalities for the module 
 *	that functiony as a black box. After the installation, you only pass a DB connection
 *	to the methods for using it with security and performance optimizations, you also, 
 *	could be throw SQL queries (MySQL) againts the table of "messages" directly, but 
 *	they could alter normal function of the system: mixture uses aren´t recomended.
 *
 * @utor: Moisés Alcocer
 * 2017, <m_alc1@hotmail.com>
 * http://www.ironwoods.es
 */

//requires
require "libs/requires.php";

use \ironwoods\modules\messages\entities\Message 	 as Message;
use \ironwoods\modules\messages\model\Counter 		 as Counter;
use \ironwoods\modules\messages\model\XDraft		 as XDraft;
use \ironwoods\modules\messages\model\XMessage		 as XMessage;
use \ironwoods\modules\messages\services\Installer	 as Installer;
use \ironwoods\modules\messages\services\Uninstaller as Uninstaller;


class Messages {

	/**********************************/
	/*** Properties declaration *******/

		private $con = NULL;


	/**********************************/
	/*** Methods declaration **********/

		/**
		 * Construct
		 * For creating an object of this class is neccesary to inyect 
		 * a connection with the database
		 * 
		 * @param	  Object		$db_con
		 * 
		 */
		public function __construct( $db_con ) {

			$this->con = $db_con;
		}


	/*** Public Methods ***************/

	////////////////////////////////////////////////////////////////////////////
	// Install the module
	// 
	// For installation you must to indicate some parameters about your DB as:
	//   - Prefix for the DB tables if exist, f.e. "appx_"
	//   - Name of client table (without prefix), this is those with the subjects that message ones 
	// to others, f.e. "users", "owners", "clients"...
	//   - Table ID, default "id"
	//   - Table ID type, f.e. "Int"
	//   - Table ID length, f.e. "12"
	// 

		/**
		 * Install the module in the App
		 * 
		 * In the module installation is required data for the table with 
		 * entities that send and receive the messages:
		 * array [
		 * 		"table-prefix"	=> "xxx",
		 * 		"table"			=> "xxx",
		 * 		"id"			=> "xxx"
		 * 		"id-type" 		=> "xxx",
		 *		"id-length" 	=> 10,
		 * ]
		 * This data is used to create settings file with an array
		 * 
		 * @param	  Array		$arr_data
		 */
		public function install( $arr_data ) {
			
			$x = new Installer( $this->con, $arr_data );
			$x->execute();
		}

	////////////////////////////////////////////////////////////////////////////
	// Uninstall the module
	// 
	// This deletes the table with the messages from the DB and the settings
	// file from the hard disk
	// 

		/**
		 * Unistalls the module from the App
		 * 
		 */
		public function uninstall() {
			
			$x = new Uninstaller( $this->con );
			$x->execute();
		}

	////////////////////////////////////////////////////////////////////////////
	// Methods in class "Count"
	//
	//
		/**
		 * Counts all of messages received
		 *
		 * @param	 int		$receiver_id 		The receiver identifier
		 * @return	 int
		 */
		public function countMsgs( $receiver_id ) {
			
			$x = new Counter( $this->con );
			return $x->all( $receiver_id );
		}

		/**
		 * Counts messages no read
		 *
		 * @param	 int		$receiver_id		The receiver identifier
		 * @return	 int
		 */
		public function countNewMsgs( $receiver_id ) {

			$x = new Counter( $this->con );
			return $x->unread( $receiver_id );
		}

	////////////////////////////////////////////////////////////////////////////
	// Methods in class "XDraft"
	//
	//
		/**
		 * Inserts a new message (draft) in the DB
		 *
		 * @param	Message		$draft
		 * @return	int			id
		 */
		public function addOneDraft( Message $draft ) {

			$x = new XDraft( $this->con );
			return $x->addOne( $draft );
		}
		/*** Alternative method for the same ***/
		public function createDraft( Message $draft ) {

			return $this->addOneDraft( $draft );
		}

		/**
		 * Changes the content for a draft
		 *
		 * @param      int		$id
		 * @param      string	$str
		 * @return     boolean
		 */
		public function changeDraftContent( $id, $str ) {

			$x = new XDraft( $this->con );
			return $x->changeContent( $id, $str );
		}

		/**
		 * Changes the subject for a draft
		 *
		 * @param      int		$id
		 * @param      string	$str
		 * @return     boolean
		 */
		public function changeDraftSubject( $id, $str ) {

			$x = new XDraft( $this->con );
			return $x->changeSubject( $id, $str );
		}

		/**
		 * Deletes a draft by the author
		 *
		 * @param 	int			$id
		 * @param 	int  		$writer_id
		 * @return	boolean
		 */
		public function deleteOneDraft( $id, $writer_id ) {

			$x = new XDraft( $this->con );
			return $x->deleteOne( $id, $writer_id );
		}

		/**
		 * Gets the drafts for the user with the ID
		 *
		 * @param 	int  		$writer_id
		 * @return	array
		 */
		public function getAllDrafts( $writer_id ) {

			$x = new XDraft( $this->con );
			return $x->getAll( $writer_id );
		}

		/**
		 * Gets the draft with the ID of the author
		 *
		 * @param   int			$id
		 * @param   int			$writer_id
		 * @return	array
		 */
		public function getOneDraft( $id, $writer_id ) {

			$x = new XDraft( $this->con );
			return $x->getOne( $id, $writer_id );
		}


	////////////////////////////////////////////////////////////////////////////
	// Methods in class "XMessages"
	//
	//
		/**
		 * Deletes a message by receiver
		 *
		 * @param 	int			$id
		 * @param 	int  		$receiver_id
		 * @return  boolean
		 */
		public function deleteOne( $id, $receiver_id ) {

			$x = new XMessage( $this->con );
			return $x->deleteOne( $id, $receiver_id );
		}

		/**
		 * Gets the messages for the user with the identifier
		 *
		 * @param 	int  		$receiver_id
		 * @return	array
		 */
		public function getAll( $receiver_id ) {

			$x = new XMessage( $this->con );
			return $x->getAll( $receiver_id );
		}
		
		/**
		 * Gets the message for the user with ID
		 *
		 * @param 	int			$id
		 * @param 	int  		$receiver_id
		 * @return	object
		 */
		public function getOne( $id, $receiver_id ) {

			$x = new XMessage( $this->con );
			return $x->getOne( $id, $receiver_id );
		}
		
		/**
		 * Gets the unread messages of the receiver
		 *
		 * @param   id			$user_id
		 * @return	array
		 */
		public function getUnread( $user_id ) {

			$x = new XMessage( $this->con );
			return $x->getUnread( $user_id );
		}

		/**
		 * Sets the message sended ( put the ID for the receiver )
		 *
		 * @param      int		$id
		 * @param      int		$destinatary_id
		 * @return     boolean
		 */
		public function send( $id, $destinatary_id ) {

			$x = new XMessage( $this->con );
			return $x->send( $id, $destinatary_id );
		}

		/**
		 * Sets the field read to 1 in the table
		 *
		 * @param      int  	$id
		 * @param      int  	$receiver_id
		 * @return     boolean
		 */
		public function setRead( $id, $receiver_id ) {

			$x = new XMessage( $this->con );
			return $x->setRead( $id, $receiver_id );
		}


	/*** Private Methods **************/



} //class
