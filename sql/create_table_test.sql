CREATE TABLE IF NOT EXISTS test(
	id			Int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	nombre		Varchar(225) NOT NULL,
	edad		Tinyint(3) NOT NULL,

	created_at	timestamp DEFAULT CURRENT_TIMESTAMP,
	deleted_at	timestamp DEFAULT 0,
		Primary Key (id)) ENGINE=InnoDB 
		DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;