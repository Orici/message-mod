/**
 * Stores the messages
 *
 */
CREATE TABLE IF NOT EXISTS messages(
	id			Int(15) UNSIGNED NOT NULL AUTO_INCREMENT,
	sender		Int(12) NOT NULL,	/* FK -> user / author */
	receiver	Int(12) DEFAULT NULL,	/* FK -> user / destinatary */
	subject		Varchar(255) NOT NULL,
	content		text NOT NULL,

	read		boolean DEFAULT FALSE,

	created_at	timestamp DEFAULT CURRENT_TIMESTAMP,
	deleted_at	timestamp DEFAULT 0,
		FOREIGN KEY (sender) REFERENCES xxx(id)
		ON DELETE CASCADE ON UPDATE CASCADE,
		FOREIGN KEY (receiver) REFERENCES xxx(id)
		ON DELETE CASCADE ON UPDATE CASCADE,
		Primary Key (id)) ENGINE=InnoDB 
		DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

/**
 *
 */