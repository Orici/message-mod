/**
 * Inserts for the Faker
 *
 */

INSERT INTO test_owners (
	id, nombre, email, created_at, deleted_at) VALUES
	(NULL, 'Carlos', 	'carlos@net.es', 	CURRENT_TIMESTAMP, 0), 
	(NULL, 'Rapetiti', 	'rapetiti@mail.es', CURRENT_TIMESTAMP, 0),
	(NULL, 'Terpe', 	'terpe@mail.es', 	CURRENT_TIMESTAMP, 0),
	(NULL, 'Erizzo', 	'erizzo@mail.es', 	CURRENT_TIMESTAMP, 0);