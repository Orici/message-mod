/**
 * Inserts for the Faker
 *
 */

INSERT INTO test_messages (
	id, sender, receiver, subject, content, read, created_at, deleted_at) VALUES 
	(NULL, '1', NULL, 'Prueba', 	'Prueba 1 -> lalala', 0, CURRENT_TIMESTAMP, 0), 
	(NULL, '1', NULL, 'Prueba 2', 	'Pruebade nuevo ', 	0, CURRENT_TIMESTAMP, 0), 
	(NULL, '1', '2', 'Prueba enviando', 'Lorem ipsum', 	0, CURRENT_TIMESTAMP, 0), 
	(NULL, '2', '3', 'Enviado y leido', 'algo', 		1, CURRENT_TIMESTAMP, 0), 
	(NULL, '4', '1', 'otro mas', 	'lalala lala', 		0, CURRENT_TIMESTAMP, 0),
	(NULL, '2', NULL, 'Prueba', 	'Prueba 1 -> lalala', 0, CURRENT_TIMESTAMP, 0), 
	(NULL, '4', NULL, 'Prueba 2', 	'Pruebade nuevo ', 	0, CURRENT_TIMESTAMP, 0), 
	(NULL, '1', '2', 'Mi xxx', 'Lorem ipsum', 				0, CURRENT_TIMESTAMP, 0), 
	(NULL, '2', '1', 'Enviado y leido', 'algo', 		1, CURRENT_TIMESTAMP, 0), 
	(NULL, '4', '3', 'otro mas xxxx', 'lalala lala', 	0, CURRENT_TIMESTAMP, 0);