<?php 

	/**
	 * Functions for traces
	 * 
	 */

		function err( $str=NULL, $exit=FALSE ) {
			/*prob( '<p class="red">' . $str . '</p>' );

			if ( $exit )
				die();*/
		}

		function prob( $str=NULL ) {
			echo $str . "<br>";
		}

		function dx( $x=NULL ) {

			echo "<pre>";
			var_dump( $x ); 	
			echo( "</pre><br>" );
		}

	/**
	 * Others
	 * 
	 */

		/**
		 * Devuelve timestand formateado para campo DATE de MySql
		 * Formato DATE: '2015-12-22 01:27:53'
		 * 
		 * @return String
		 */
		function now() {

			return date( "Y-m-d h:i:s" );
		}

		
		function print_styles() {

			echo "<style>";
			echo "h1,h2,h3,h4 { font-size: 1.4em; margin: 0 0 -5px }";
			echo ".blue { color:blue }";
			echo ".green { color:green }";
			echo ".red { color:red }";
			echo "</style>";
		}
		

