<?php 

/**
 * @file: requires.php
 *
 * @utor: Moisés Alcocer
 * 2017, <m_alc1@hotmail.com>
 * http://www.ironwoods.es
 */

require_once "libs/functions.php";
require_once "config/config.php";
require_once "entities/message.php";
require_once "helpers/dbvalidator.php";
require_once "helpers/files.php";
require_once "helpers/paramsvalidator.php";
require_once "helpers/sqlcomposer.php";
require_once "libs/model.php";
require_once "model/counter.php";
require_once "model/faker.php";
require_once "model/xdraft.php";
require_once "model/xmessage.php";
require_once "services/installer.php";
require_once "services/uninstaller.php";