<?php namespace ironwoods\modules\messages\libs;

/**
 * @file: model.php
 * @info: Class that implements common methods to handle DB
 *
 * @utor: Moisés Alcocer
 * 2017, <m_alc1@hotmail.com>
 * http://www.ironwoods.es
 */

use \ironwoods\modules\messages\config\Config as Config;
use \ironwoods\modules\messages\helpers\ParamsValidator as PVal;
use \ironwoods\modules\messages\helpers\SqlComposer as SqlComposer;


class Model {

	/**********************************/
	/*** Properties declaration *******/

		protected $con = NULL;

		protected $id	 = NULL;
		protected $table = NULL;


	/**********************************/
	/*** Methods declaration **********/

		/**
		 * Construct
		 *
		 * @param	Connection	$con		The connection with the DB
		 * @param	array		$arr_data	Only for the installing proccess
		 */
		protected function __construct( $con=NULL, $arr_data=NULL ) {
			//echo "<pre>"; var_dump( $con );die();
			if ( PVal::connection( $con )) {
				$this->con = $con;
				$this->setTableProperties( $arr_data );

			} else
				err( "Model / __construct() -> Err args", TRUE );
		}

	/*** Protected Methods ************/

		/**
		 * Runs a query againts the DB and returns the results
		 *
		 * @param		array		$arr
		 * @return  	mixed
		 */
		protected function runQueries( $arr=NULL, $params=NULL ) {
			//prob( "Model / runQueries()" );
			
			if ( $arr && is_array( $arr )) {
				$arr_res = array();

				foreach ( $arr as $key => $sql ) {

					$res = ( $params )
						? $this->runQuery( $sql, $params[ $key ] )
						: $this->runQuery( $sql );
					
					if ( $res )
						$arr_res[] = $res;

					else {
						err( "Model / runQueries() -> Err. Ejecutadas: " . $key );

						return FALSE;
					}
				}
				//prob( "¡ Queries ejecutadas con éxito !" );


				//Returns query results
				return $arr_res;

			} else
				//prob( "Model / runQueries() -> Err args" );
			

			return FALSE;
		}

		/**
		 * Runs a query againts the DB and returns the results
		 *
		 * @param		string		$sql
		 * @return  	mixed
		 */
		protected function runQuery( $sql=NULL, $params=NULL ) {
			//prob( "Model / runQuery()" );
			//prob( "SQL: <span class='blue'>" . $sql . "</span><hr>" );
			////prob( "Parámetros: " ); var_dump( $params ); //die();
			////prob( "Conexión: " ); var_dump($this->con); //die();
			
			if ( strpos( $sql, "DELETE" ) === 0 )
				return $this->con->exec( $sql ); //exec()
				 

			//If the query use binding -> use function: bind() 
			//f.e. $sql = "INSERT INTO db_fruit (id, type) VALUES (? ,?)"
			$query = ( $params )
				? $this->bind( $sql, $params )
				: $this->con->prepare( $sql );

			if ( $query )
				$query->execute();
			
			else
				err( "Model / runQuery() -> Err: Query vacia" );

			////prob( "Devolviendo resultados..." );
			//var_dump( $query );
			
			if ( strpos( $sql, "INSERT" ) === 0 )
				return (int) $this->con->lastInsertId();

			if ( $query && strpos( $sql, "UPDATE" ) === 0 )
				return $query->rowCount();

			//prob( "Devolviendo resultados para SELECT..." );
			return $query;
		}


	/*** Private Methods **************/

		/**
		 * Binds the params
		 *
		 * @param 		$sql
		 * @param 		$params
		 * @return 		$query
		 */
		private function bind( $sql, $params ) {
			//prob( "Model / bind()" );

			foreach ( $params as $key => $value ) {
				if ( is_null( $value ))
					$params[ $key ] = "NULL";
			}

			$query = $this->con->prepare( $sql );
			
			if ( $query ) {
				$num = count( $params );
				$pos = 0;

				for ( $i=1; $i <= $num; $i++ ) { 
					//$query->bindParam(1, $newId)
					$param = $params[ $pos ];
					//prob( "Parámetro " . $i . " -> " . $param );

					$validation = $this->getValidateType( $param );
					$query->bindValue( $i, $param, $validation );

					$pos++;
				}
				var_dump( $query ); echo "<hr>";
				//$query->execute( $params );
				
			} else
				err( "Model / bind() -> Err prepare SQL" );
				

			return $query;
		}

		/**
		 * Gets the validate type for a param
		 *
		 * @param      mixed	$param  The parameter
		 * @return     <type>  The validate type.
		 */
		private function getValidateType( $param ) {
			$validation = FALSE;

			if ( is_int( $param ))
				$validation = \PDO::PARAM_INT;

			elseif ( is_bool( $param ))
				$validation = \PDO::PARAM_BOOL;

			elseif ( is_null( $param ) || $param == "NULL" )
				$validation = \PDO::PARAM_NULL; 

			elseif ( is_string( $param ))
				$validation = \PDO::PARAM_STR;


			return $validation;
		}

		/**
		 * Gets the table from settings and sets the propertie $table
		 *
		 */
		private function setTableProperties( $arr_data=NULL ) {
			//prob( "Model / setTableProperties()" );

			$table = "messages";

			if ( $arr_data )
				$data = $arr_data;

			else {
				$cfg = new Config();
				$data = $cfg->getData();
				////prob("<pre>"); var_dump( $data );
			}
				

			$this->id 	 = $data[ "id" ];
			$this->table = $data[ "table-prefix" ] . $table;
		}

} //class
